﻿using System;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.IO;
using TJFramework;
using TJStandard;
using TJSettings;
using static TJExampleSettingsConsumer.Program;
using static TJFramework.TJFrameworkManager;
using System.Data.SQLite;

namespace TJAdminSettings
{
  public partial class FormTest3 : RadForm, IEventStartWork, IOutputMessage
  {
    private ManagerOfLocalDatabase DbSettings { get => Manager.DbSettings; }

    public FormTest3()
    {
      InitializeComponent();
    }

    public void EventStartWork()
    {
      BxTest.Click += EventTest3;
    }

    private void EventTest1(object sender, EventArgs e)
    {
      try
      {
        TestLocalDatabase();
      }
      catch (Exception ex)
      {
        Print("ERROR!");
        Print(ex.Message);
      }
    }

    private void EventTest2(object sender, EventArgs e)
    {
      long IdFolder = CxConvert.ToInt64(TxTest.Text, 0);
      string result = string.Empty;
      Stopwatch sw = Stopwatch.StartNew();
      for (int i = 0; i < 21; i++)
        Print($"{i} --- {DbSettings.GetFullPath(i)}");
      sw.Stop();
      Print($"ms = {sw.ElapsedMilliseconds}");    
    }

    private void EventTest3(object sender, EventArgs e)
    {
      long IdFolder = CxConvert.ToInt64(TxTest.Text, 0);
      string result = string.Empty;
      Stopwatch sw = Stopwatch.StartNew();
      for (int i = 0; i < 21; i++)
        Print($"{i} --- {DbSettings.GetIdParent(i)}");
      sw.Stop();
      Print($"ms = {sw.ElapsedMilliseconds}");
    }

    public void OutputMessage(string message, string header = "") => Debug(message, header);

    public void Debug(string message, string header = "")
    {
      TxMessage.AppendText((header + " " + message).Trim() + Environment.NewLine);
    }

    public void Print(string message) => Debug(message, CxConvert.Time);


    public void Check(ReturnCode code)
    {
      if (code.Error) throw new Exception("An error has occured.");
    }

    public void TestLocalDatabase()
    {
      string DatabaseFileName = "test_database.sqlite3";
      DatabaseFileName = Path.Combine(Application.StartupPath, DatabaseFileName);

      if (File.Exists(DatabaseFileName))
      {
        Print($"Test canceled. File already exists {DatabaseFileName}"); return;
      }

      ReturnCode code = DbSettings.CreateNewDatabase(DatabaseFileName); Check(code);
      string root = DbSettings.GetRootFolderName();
      code = DbSettings.FolderInsert(root, "Test_Folder_1"); Check(code);
      code = DbSettings.FolderInsert(string.Empty, "Test_Folder_2"); Check(code);

      SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder();     
    }
  }
}
