﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bogus;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using TJFramework;
using TJSettings;
using TJStandard;
using TJStandard.Tools;
using static TJExampleSettingsConsumer.Program;
using static TJFramework.TJFrameworkManager;

namespace TJExampleSettingsConsumer
{
  public partial class FxTest1 : RadForm, IEventStartWork, IEventEndWork
  {
    private ManagerOfLocalDatabase Db { get => Manager.DbSettings;}

    System.Windows.Forms.Timer Timer1 { get; } = new System.Windows.Forms.Timer();

    System.Windows.Forms.Timer Timer2 { get; } = new System.Windows.Forms.Timer();

    System.Windows.Forms.Timer Timer3 { get; } = new System.Windows.Forms.Timer();

    System.Windows.Forms.Timer Timer4 { get; } = new System.Windows.Forms.Timer();


    public FxTest1()
    {
      InitializeComponent();
    }

    public void OutputMessage(string message, string header = "") => Debug(message, header);

    public void Debug(string message, string header = "")
    {
      TxMessage.AppendText((header + " " + message).Trim() + Environment.NewLine);
    }

    public void Print(string message) => Debug(message, CxConvert.Time);

    public void EventStartWork()
    {      
      TxFolderName.ZzRightButtonClick(TestReadValue);
      TxSettingName.ZzRightButtonClick(TestReadValue);
      TxFolderName.Text = @"Branch\Test\Folder\Server\Tcp";
      BxTestAutoChangeSetting.Click += EventTestAutoChangeSetting;
    }

    private void TestReadValue(object sender, EventArgs e)
    {
      ReceivedValueText response = Db.GetSettingText(TxFolderName.Text.Trim(), TxSettingName.Text.Trim());
      Print("Received value ---------------------------------------------------------------");
      Print(response.Value);
      Print($"Return Code = {ReturnCodeFormatter.ToString(response.Code)}");
    }

    private void EventTestAutoChangeSetting(object sender, EventArgs e)
    {
      Print("Started !");
      //ConfigureTimerChangingInteger(Timer1, @"\Branch\Test\Folder\Server\Tcp\", "integer1", 14500, 1, 4009);
      ConfigureTimerChangingInteger(Timer2, @"Root123\Branch\Test\Folder\Server\Tcp\", "integer2", 2000000, 1, 5000);
      //ConfigureTimerChangingText(Timer3, @"\Branch\Test\Folder\Server\Tcp", "text1", 6117);
      //ConfigureTimerChangingText(Timer4, @"Branch\Test\Folder\Server\Tcp", "text4", 7119);
    }

    private void ConfigureTimerChangingInteger(System.Windows.Forms.Timer timer, string FolderPath, string IdSetting, long StartValue, long Step, int IntervalMilliseconds)
    {
      long value = StartValue;
      timer.Interval = IntervalMilliseconds;
      timer.Tick += (s, e) =>
      {
        value = value + Step;
        ReceivedValueInteger64 res = Db.GetSettingInteger64(FolderPath, IdSetting);
        ReturnCode code = Db.SaveSettingInteger64(FolderPath, IdSetting, value);
        if (code.Error) Ms.Message("Timer reports an error ", $"{code.Message} {code.Note}").NoAlert().Error();
      };
      timer.Start();
    }

    private void ConfigureTimerChangingText(System.Windows.Forms.Timer timer, string FolderPath, string IdSetting, int IntervalMilliseconds)
    {
      Faker faker = new Faker();
      uint x = faker.Random.UInt(0,100000);
      string value = "";
      timer.Interval = IntervalMilliseconds;
      timer.Tick += (s, e) =>
      {
        value = x++.ToString() + " xa xa xa";
        //ReceivedValueInteger64 res = Db.GetSettingText(FolderPath, IdSetting);
        ReturnCode code = Db.SaveSettingText(FolderPath, IdSetting, TypeSetting.Text, value);
        if (code.Error) Ms.Message("Timer reports an error ", $"{code.Message} {code.Note}").NoAlert().Error();
      };
      timer.Start();
    }

    public void EventEndWork()
    {
      Timer1.Stop();
      Timer2.Stop();
      Timer3.Stop();
      Timer4.Stop();
    }
  }
}
