﻿namespace TJExampleSettingsConsumer
{
    partial class FxTest1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.PvTest = new Telerik.WinControls.UI.RadPageView();
      this.PgTestOne = new Telerik.WinControls.UI.RadPageViewPage();
      this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
      this.TxMessage = new Telerik.WinControls.UI.RadTextBox();
      this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
      this.TxFolderName = new Telerik.WinControls.UI.RadButtonTextBox();
      this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
      this.TxSettingName = new Telerik.WinControls.UI.RadButtonTextBox();
      this.radButtonElement2 = new Telerik.WinControls.UI.RadButtonElement();
      this.BxTestAutoChangeSetting = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.PvTest)).BeginInit();
      this.PvTest.SuspendLayout();
      this.PgTestOne.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
      this.radPanel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
      this.radPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolderName)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxSettingName)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxTestAutoChangeSetting)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvTest
      // 
      this.PvTest.Controls.Add(this.PgTestOne);
      this.PvTest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PvTest.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PvTest.Location = new System.Drawing.Point(0, 0);
      this.PvTest.Name = "PvTest";
      this.PvTest.SelectedPage = this.PgTestOne;
      this.PvTest.Size = new System.Drawing.Size(1100, 736);
      this.PvTest.TabIndex = 1;
      // 
      // PgTestOne
      // 
      this.PgTestOne.Controls.Add(this.radPanel2);
      this.PgTestOne.Controls.Add(this.radPanel1);
      this.PgTestOne.ItemSize = new System.Drawing.SizeF(65F, 28F);
      this.PgTestOne.Location = new System.Drawing.Point(10, 37);
      this.PgTestOne.Name = "PgTestOne";
      this.PgTestOne.Size = new System.Drawing.Size(1079, 688);
      this.PgTestOne.Text = " T E S T";
      // 
      // radPanel2
      // 
      this.radPanel2.Controls.Add(this.TxMessage);
      this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.radPanel2.Location = new System.Drawing.Point(0, 183);
      this.radPanel2.Name = "radPanel2";
      this.radPanel2.Size = new System.Drawing.Size(1079, 505);
      this.radPanel2.TabIndex = 0;
      // 
      // TxMessage
      // 
      this.TxMessage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.TxMessage.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxMessage.Location = new System.Drawing.Point(0, 0);
      this.TxMessage.Multiline = true;
      this.TxMessage.Name = "TxMessage";
      // 
      // 
      // 
      this.TxMessage.RootElement.StretchVertically = true;
      this.TxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.TxMessage.Size = new System.Drawing.Size(1079, 505);
      this.TxMessage.TabIndex = 0;
      // 
      // radPanel1
      // 
      this.radPanel1.Controls.Add(this.BxTestAutoChangeSetting);
      this.radPanel1.Controls.Add(this.TxFolderName);
      this.radPanel1.Controls.Add(this.TxSettingName);
      this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.radPanel1.Location = new System.Drawing.Point(0, 0);
      this.radPanel1.Name = "radPanel1";
      this.radPanel1.Size = new System.Drawing.Size(1079, 183);
      this.radPanel1.TabIndex = 0;
      // 
      // TxFolderName
      // 
      this.TxFolderName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxFolderName.AutoSize = false;
      this.TxFolderName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxFolderName.Location = new System.Drawing.Point(16, 30);
      this.TxFolderName.Name = "TxFolderName";
      this.TxFolderName.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement1});
      this.TxFolderName.Size = new System.Drawing.Size(1050, 28);
      this.TxFolderName.TabIndex = 0;
      // 
      // radButtonElement1
      // 
      this.radButtonElement1.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.radButtonElement1.Name = "radButtonElement1";
      this.radButtonElement1.Text = "     Start test     ";
      this.radButtonElement1.UseCompatibleTextRendering = false;
      // 
      // TxSettingName
      // 
      this.TxSettingName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxSettingName.AutoSize = false;
      this.TxSettingName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxSettingName.Location = new System.Drawing.Point(16, 73);
      this.TxSettingName.Name = "TxSettingName";
      this.TxSettingName.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement2});
      this.TxSettingName.Size = new System.Drawing.Size(1050, 28);
      this.TxSettingName.TabIndex = 0;
      // 
      // radButtonElement2
      // 
      this.radButtonElement2.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.radButtonElement2.Name = "radButtonElement2";
      this.radButtonElement2.Text = "     Start test     ";
      this.radButtonElement2.UseCompatibleTextRendering = false;
      // 
      // BxTestAutoChangeSetting
      // 
      this.BxTestAutoChangeSetting.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxTestAutoChangeSetting.Location = new System.Drawing.Point(16, 118);
      this.BxTestAutoChangeSetting.Name = "BxTestAutoChangeSetting";
      this.BxTestAutoChangeSetting.Size = new System.Drawing.Size(199, 35);
      this.BxTestAutoChangeSetting.TabIndex = 1;
      this.BxTestAutoChangeSetting.Text = "Test auto change setting";
      // 
      // FxTest1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1100, 736);
      this.Controls.Add(this.PvTest);
      this.Name = "FxTest1";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "FxTest1";
      ((System.ComponentModel.ISupportInitialize)(this.PvTest)).EndInit();
      this.PvTest.ResumeLayout(false);
      this.PgTestOne.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
      this.radPanel2.ResumeLayout(false);
      this.radPanel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
      this.radPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TxFolderName)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxSettingName)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxTestAutoChangeSetting)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private Telerik.WinControls.UI.RadPageView PvTest;
    private Telerik.WinControls.UI.RadPageViewPage PgTestOne;
    private Telerik.WinControls.UI.RadPanel radPanel2;
    private Telerik.WinControls.UI.RadTextBox TxMessage;
    private Telerik.WinControls.UI.RadPanel radPanel1;
    private Telerik.WinControls.UI.RadButtonTextBox TxFolderName;
    private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
    private Telerik.WinControls.UI.RadButtonTextBox TxSettingName;
    private Telerik.WinControls.UI.RadButtonElement radButtonElement2;
    private Telerik.WinControls.UI.RadButton BxTestAutoChangeSetting;
  }
}
