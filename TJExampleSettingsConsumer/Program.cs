﻿using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using TestProject;
using TJAdminSettings;
using TJFramework;
using TJFramework.FrameworkSettings;
using TJSettings;
using TJSettingsFormTreeView;

/*
1. Install package "SQLite" by SQLite Development team [ver 3.13; date=2016-10-19]
2. Install package "System.Data.SQLite.Core" by SQLite Development team [ver 1.0.112; date=2019-10-28]
3. Make sure option [Copy Local] = True in property of "System.Data.SQLite" reference
4. Make sure that the two folders "x64" and "x86" containting "SQLite.Interop.dll" are in the application folder after the first building.
5. After adding a new project, go to the "Configuration Properties" section of the [Solution Properties] and select the "Build" checkmark for the new project.
*/

namespace TJExampleSettingsConsumer
{
  static class Program
  {
    public static string ApplicationUniqueName { get; } = "TJSettings: example of use";

    private static Mutex AxMutex = null;

    public static ProjectManager Manager { get; set; } = null;

    public static ManagerOfLocalDatabase DbSettings { get => Manager.DbSettings; }

    // The member of this class must be an inheritor of the [FormTreeViewApplicationSettings] class //
    public static CxApplicationSettings ApplicationSettings { get => TJFrameworkManager.ApplicationSettings<CxApplicationSettings>(); } // User custom settings in Property Grid //

    public static TJStandardFrameworkSettings FrameworkSettings { get; } = TJFrameworkManager.FrameworkSettings; // Framework embedded settings //

    private static void TestEventPageChanged(string PageName)
    {
      MessageBox.Show("You have changed a page = " + PageName);
    }

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      AxMutex = new Mutex(true, ApplicationUniqueName, out bool createdNew);
      if (!createdNew)
      {
        MessageBox.Show("Another instance of the application is already running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      TJFrameworkManager.Logger.FileSizeLimitBytes = 1000000;
      TJFrameworkManager.Logger.Create(Assembly.GetExecutingAssembly().GetName().Name);

      TJFrameworkManager.Service.CreateApplicationSettings<CxApplicationSettings>(Assembly.GetExecutingAssembly().GetName().Name);
      
      TJFrameworkManager.Service.AddForm<FxTest1>("Test 1-1");
      TJFrameworkManager.Service.AddForm<FormTest1>("Test 1");
      TJFrameworkManager.Service.AddForm<FormTest2>("Test 2");
      TJFrameworkManager.Service.AddForm<FormTest3>("Test 3");
      TJFrameworkManager.Service.AddForm<FormTreeView>("Advanced settings");
      TJFrameworkManager.Service.StartPage<FxTest1>();

      TJFrameworkManager.Service.SetMainFormCaption(ApplicationUniqueName);
      //TJFrameworkManager.Service.SetMainPageViewOrientation(StripViewAlignment.Left);

      FrameworkSettings.HeaderFormSettings = "Settings";
      FrameworkSettings.HeaderFormLog = "Message log";
      FrameworkSettings.HeaderFormExit = "Exit";
      //FrameworkSettings.ConfirmExitButtonText = "Confirm exit";

      FrameworkSettings.MainFormMinimizeToTray = false;
      FrameworkSettings.VisualEffectOnStart = true;
      FrameworkSettings.VisualEffectOnExit = true;

      FrameworkSettings.ValueColumnWidthPercent = 60;
      FrameworkSettings.MainPageViewReducePadding = true;
      FrameworkSettings.RememberMainFormLocation = true;
      FrameworkSettings.PageViewFont = new Font("Verdana", 9);

      FrameworkSettings.MaxAlertCount = 3;
      FrameworkSettings.LimitNumberOfAlerts = true;
      FrameworkSettings.SecondsAlertAutoClose = 5;
      FrameworkSettings.FontAlertCaption = new Font("Verdana", 9);
      FrameworkSettings.FontAlertText = new Font("Verdana", 9);

      Manager = ProjectManagerFactory.Create(true, true); // Второй аргумент означает, что нужно уведомлять даже о неудачных попытках действий, а не только об успешных //

      TJFrameworkManager.Service.EventPageChanged += Manager.EventPageChanged;
      TJFrameworkManager.Service.EventAfterAllFormsAreCreated += Manager.EventAfterAllFormsAreCreated;
      TJFrameworkManager.Service.EventBeforeMainFormClose += Manager.EventBeforeMainFormClose;
 
      Manager.MnFormTreeView = ManagerFormTreeViewFactory.Create(ApplicationSettings, FormTreeViewMode.OneLocalSettingsDatabase);
      
      /*
      Action ExampleOfVisualSettingsAndEvents = () =>
      {
        FrameworkSettings.MainFormMargin = 50;
        FrameworkSettings.PageViewItemSize = new Size(200, 30);
        FrameworkSettings.ItemSizeMode = PageViewItemSizeMode.EqualSize;
        FrameworkSettings.PageViewItemSpacing = 50;
        FrameworkSettings.PropertyGridPadding = new Padding(155, 100, 45, 25);
      };
      */

      /*
      TJFrameworkManager.Service.EventPageChanged += Manager.MnFormTreeView.EventPageChanged;
      TJFrameworkManager.Service.EventAfterAllFormsAreCreated += Manager.MnFormTreeView.EventAfterAllFormsAreCreated;
      TJFrameworkManager.Service.EventBeforeMainFormClose += Manager.MnFormTreeView.EventBeforeMainFormClose;
      */

      TJFrameworkManager.Run();
    }
  }
}