﻿using System.Windows.Forms;
using TJSettings;
using TJStandard;
using static TJExampleSettingsConsumer.Program;

namespace TJExampleSettingsConsumer
{
  public class ProjectManagerFactory
  {
    public static ProjectManager Create(bool SubscribeToEventsOfLocalSettingsDatabase, bool NotifyEvenInCaseOfError)
    {
      ProjectManager manager = new ProjectManager();
      if (manager.DbSettings == null)
        manager.DbSettings = ManagerOfLocalDatabase.Create(manager, SubscribeToEventsOfLocalSettingsDatabase, nameof(ProjectManager), NotifyEvenInCaseOfError);
      
      /*   
      No need to instantly connect to the database.

      The user can perform the connection operation later.

      ReturnCode code = manager.DbSettings.ConnectToDatabase(ApplicationSettings.SettingsDatabaseLocation);
      
      if (code.Error)
      {
        MessageBox.Show("Could not connect to the database of settings", code.StringValue + " " + code.StringNote, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      */

      return manager;
    }
  }
}