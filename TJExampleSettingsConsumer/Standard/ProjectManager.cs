﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Drawing;
using TJSettings;
using TJStandard;
using TJFramework;
using TJSettingsFormTreeView;
using static TJFramework.TJFrameworkManager;

namespace TJExampleSettingsConsumer
{
  public class ProjectManager : IOutputMessage, ILocalDatabaseOfSettingsEventNotification
  {
    public const string Empty = "";

    public Size MessageSize { get; } = new Size(400, 300);

    public ManagerFormTreeView MnFormTreeView { get; internal set; } = null;

    public ManagerOfLocalDatabase DbSettings { get; internal set; } = null; 

    public void OutputMessage(string message, string header = Empty)
    {
      Ms.Message(header, message).NoAlert().Debug();
    }

    internal void EventPageChanged(string name)
    {

    }

    internal void EventAfterAllFormsAreCreated()
    {

    }

    internal void EventBeforeMainFormClose()
    {

    }

    public void EventNotification(Notification msg, ReturnCode code, object subscriber, string subscriberName)
    {  
      string header = $"manager reports: Type={msg.TypeOfEvent};\nIdFolder={msg.IdFolder}";
      string message = $"Success={code.Success}\nInitiator={msg.Initiator}\nType={(TypeSetting)msg.IdTypeOfSetting}\nValue={msg.Value}";
      //Ms.Message(header, message).Pos(MsgPos.TopRight).Delay(30).CloseOnClick().Info();    
    }
  }
}