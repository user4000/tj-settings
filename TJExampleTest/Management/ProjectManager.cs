﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJStandard;
using static TJFramework.TJFrameworkManager;

namespace TJExampleTest
{
  public class ProjectManager : IOutputMessage
  {
    public const string Empty = "";

    public ManagerOfActorSystem MnActorSystem { get; } = new ManagerOfActorSystem();

    public void OutputMessage(string message, string header = Empty)
    {
      Ms.Message(message, header).NoAlert().Debug();
    }

    internal void EventPageChanged(string name)
    {

    }

    internal void EventAfterAllFormsAreCreated()
    {
      MnActorSystem.Configure();
      Pages.GetRadForm<FxTest1>().Configure();
    }

    internal void EventBeforeMainFormClose()
    {

    }
  }
}