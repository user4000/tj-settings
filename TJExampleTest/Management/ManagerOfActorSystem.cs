﻿using Akka.Actor;
using Akka.Configuration;
using TJFramework;
using static TJFramework.TJFrameworkManager;
using static TJExampleTest.Program;

namespace TJExampleTest
{
  public class ManagerOfActorSystem
  {
    public ManagerOfActorSystem()
    {

    }

    public void Configure()
    {
      string StringConfig = @"
      akka {
          actor {
              provider = remote
          }
          remote {
              enabled-transports = [""akka.remote.dot-netty.tcp""]
              dot-netty.tcp {
                  maximum-frame-size = 4000000b
                  port = " + Program.ApplicationSettings.PortNumberAkkaSystem.ToString() + @"
                  hostname = localhost
              }
          }
      }";

      var config = ConfigurationFactory.ParseString(StringConfig);

      AcMain = ActorSystem.Create("MainActorSystem", config);

      Ms.Message("Actor system is launched", AcMain.Name).NoAlert().Debug();
    }
  }
}