﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Akka.Actor;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJFramework;
using TJStandard;
using static TJExampleTest.Program;
using static TJFramework.TJFrameworkManager;

namespace TJExampleTest
{
  public partial class FxTest1 : RadForm, IEventStartWork, IOutputMessage
  {
    public IActorRef AxCommander { get; set; } = null;

    public ActorSelection RemoteActors { get; set; } = null;

    public FxTest1()
    {
      InitializeComponent();
    }

    public void EventStartWork()
    {
      TxTest.ZzRightButtonClick(EventButtonTest);
      BxCheckRemoteActor.Click += async (s, e) => await EventButtonCheckRemoteActors(s, e);
    }

    private void EventButtonTest(object sender, EventArgs e)
    {            
      //TJStandard.Actor.AskStringMessage message = new TJStandard.Actor.AskStringMessage(TxTest.Text);
      Tuple<AskStringMessage, string> message = 
        new Tuple<AskStringMessage, string>
        (
          new AskStringMessage("kyky"), 
          CxConvert.ObjectToJson(new AskStringMessage(TxTest.Text))
        );
      TxTest.Clear();
   
      RemoteActors.Tell(message);

      Print("Message sent: " + message);
    }

    private async Task EventButtonCheckRemoteActors(object sender, EventArgs e)
    {
      Print("Checking...");
      IActorRef x = await RemoteActors.ResolveOne(new TimeSpan(0, 0, 3));
      TxMessage.Clear();
      Print("Ok = " + x.Path);
    }

    public void Configure()
    {     
      AxCommander = AcMain.ActorOf<AcCommander>(nameof(AcCommander));

      //AxCommander = AcMain.ActorOf(AcCommander.Props(), nameof(AcCommander));
      Print(AxCommander.Path.ToStringWithAddress());

      AxCommander.Tell(Program.Manager);
      AxCommander.Tell(this);
      AxCommander.Tell(new AskStringMessage("hello!"));

      string path = $"akka.tcp://{AcMain.Name}@localhost:{Program.ApplicationSettings.PortNumberRemoteAkkaSystem}/user/{nameof(AcCommander)}";
      Print(path);
      RemoteActors = AcMain.ActorSelection(path);
    }

    public void OutputMessage(string message, string header = "") => Debug(message, header);

    public void Debug(string message, string header = "")
    {
      if (TxMessage.InvokeRequired)
        TxMessage.Invoke((MethodInvoker)delegate { PrintInner(message, header); });
      else
        PrintInner(message, header);
    }

    private void PrintInner(string message, string header = "")
    {
      TxMessage.AppendText((header + " " + message).Trim() + Environment.NewLine);
    }

    public void Print(string message) => Debug(message, CxConvert.Time);

  }
}
