﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using TJFramework;
using TJFramework.ApplicationSettings;
using static TJExampleTest.Program;
using static TJFramework.Logger.Manager;

namespace TJExampleTest
{
  [Serializable] 
  public class CxApplicationSettings : TJStandardApplicationSettings
  {
    [Category("User interface")]
    [DisplayName("Orientation tabs of the main form")]
    public StripViewAlignment MainPageOrientation { get; set; } = StripViewAlignment.Top;

    [Category("Network")]
    [DisplayName("Akka local system port")]
    [Description("Akka local system port")]
    public int PortNumberAkkaSystem { get; set; } = 17002;

    [Category("Network")]
    [DisplayName("Akka remote system port")]
    [Description("Akka remote system port")]
    public int PortNumberRemoteAkkaSystem { get; set; } = 17003;

    public override void PropertyValueChanged(string PropertyName)
    {
      if (PropertyName == nameof(MainPageOrientation))
        TJFrameworkManager.Service.SetMainPageViewOrientation(MainPageOrientation);
    }

    public override void EventBeforeSaving()
    {

    }

    public override void EventAfterSaving()
    {

    }
  }
}



