﻿using System;

namespace TJExampleTest
{
  [Serializable]
  public class AmStringMessage
  {
    public string Message { get; set; } = string.Empty;

    public AmStringMessage(string text)
    {
      Message = text;
    }

    //public static AmStringMessage Create(string text) => new AmStringMessage(text);
  }
}