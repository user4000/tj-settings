﻿using System;

namespace TJExampleTest
{
  [Serializable]
  public class AskStringMessage
  {
    public string Message { get; set; } = string.Empty;

    public AskStringMessage(string text)
    {
      Message = text;
    }

    //public static AskStringMessage Create(string text) => new AskStringMessage(text);
  }
}