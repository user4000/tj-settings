﻿using System;
using System.Diagnostics;
using Akka.Actor;
using TJStandard;

namespace TJExampleTest
{
  public class AcMessageProcessor : ReceiveActor
  {
    public AcMessageProcessor()
    {
      Receive<AskStringMessage>(arg => EventProcessRequestMessage(arg));
      Receive<AmStringMessage>(arg => EventProcessResponseMessage(arg));
      Receive<Tuple<AskStringMessage, string>>(obj => EventAskStringMessage(obj));
      Receive<string>(arg => EventProcessString(arg));
    }

    private void EventAskStringMessage(Tuple<AskStringMessage, string> obj)
    {

      AskStringMessage req;
      string msg = "";
      
      try
      {
        req = CxConvert.JsonToObject<AskStringMessage>(obj.Item2);
        msg = req.Message;
      }
      catch (Exception ex)
      {
        msg = ex.Message + " " + ex.Source;
      }
  
      Tuple<AmStringMessage, string> res = 
        new Tuple<AmStringMessage, string>
        (
          new AmStringMessage(string.Empty), 
          CxConvert.ObjectToJson(new AmStringMessage("You sent me = " + msg))
        );
      Sender.Tell(res);
    }

    private void EventProcessString(string arg)
    {
      Sender.Tell("You have sent me a message: " + arg);
    }

    private void EventProcessRequestMessage(AskStringMessage msg)
    {
      Trace.WriteLine($"Worker received a Request: {msg.Message}");
      AmStringMessage response = new AmStringMessage($"You have asked a question = {msg.Message}. My response is = {DateTime.Now}");
      Sender.Tell(response);
    }

    private void EventProcessResponseMessage(AmStringMessage arg)
    {
      Trace.WriteLine($"Worker received a Response: {arg.Message}");
      AmStringMessage response = new AmStringMessage($"Response ---> {arg.Message}");
      Sender.Tell(response);
    }
  }
}