﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Akka.Actor;
using TJStandard;

namespace TJExampleTest
{
  public class AcCommander : ReceiveActor
  {
    private ProjectManager Manager { get; set; } = null;

    private IActorRef AxMessageProcessor { get; set; } = null;

    private IOutputMessage Printer { get; set; } = null;

    public AcCommander()
    {
      Receive<ProjectManager>(obj => EventApplicationManager(obj));
      Receive<IOutputMessage>(obj => EventObjectPrinter(obj));
      Receive<AskStringMessage>(obj => EventRequestMessageHasCome(obj));
      Receive<AmStringMessage>(obj => EventResponseMessageHasCome(obj));
      Receive<Tuple<AskStringMessage, string>>(obj => EventAskStringMessage(obj));
      Receive<Tuple<AmStringMessage, string>>(obj => EventAmStringMessage(obj));
      Receive<string>(obj => EventString(obj));
    }

    // TODO: Разобраться с Props - как нужно создавать актор через Props так чтобы передать в этот метод аргументы которые инициализируют члены класса //
    /*public static Props Props()
    {
      return Akka.Actor.Props.Create(() => new AcCommander());
    }*/

    private void EventAmStringMessage(Tuple<AmStringMessage, string> obj)
    {
      AmStringMessage msg = CxConvert.JsonToObject<AmStringMessage>(obj.Item2);
      if (Printer != null) Printer.OutputMessage(msg.Message);
      if (Manager != null) Manager.OutputMessage(msg.Message);
    }

    private void EventAskStringMessage(Tuple<AskStringMessage, string> obj)
    {
      AxMessageProcessor.Tell(obj);
      //if (Printer != null) Printer.OutputMessage("Commander: " + obj.Item2);
    }

    private void EventString(string obj)
    {
      if (Printer != null) Printer.OutputMessage(obj);
      if (Manager != null) Manager.OutputMessage(obj);
    }

    private void EventRequestMessageHasCome(AskStringMessage obj)
    {
      //MessageBox.Show("I receive a request!");
      //Trace.WriteLine($"Commander received a request: {obj.Message}");
      //string message = CxConvert.ObjectToJson(obj);
    
      AxMessageProcessor.Tell(obj);
    }

    private void EventResponseMessageHasCome(AmStringMessage obj)
    {
      //Trace.WriteLine($"Commander received a Response: {obj.Message}");
      if (Printer != null) Printer.OutputMessage(obj.Message);
      if (Manager != null) Manager.OutputMessage(obj.Message);
    }

    private void EventObjectPrinter(IOutputMessage obj)
    {
      Printer = obj;
      if (Printer != null) Printer.OutputMessage($"Nice to meet you Printer: {obj.ToString()}");
      else Printer.OutputMessage("ERROR ! Printer is NULL ");
    }

    private void CreateChildActors()
    {
      AxMessageProcessor = Context.ActorOf<AcMessageProcessor>(nameof(AcMessageProcessor));
      AxMessageProcessor.Tell(new AskStringMessage("Hey worker"));
    }

    private void EventApplicationManager(ProjectManager obj)
    {
      Manager = obj;
      CreateChildActors();
      Manager = obj;
      if (Manager != null) Manager.OutputMessage($"Are you a manager? {obj.ToString()}");
      else Manager.OutputMessage("ERROR ! Manager is NULL ");
    }
  }
}