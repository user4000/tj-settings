﻿using System;

namespace TJSettings
{
  [Serializable]
  public class RowFolder
  {
    public long IdFolder { get; set; }

    public long IdParent { get; set; }

    public string NameFolder { get; set; }
  }
}

