﻿using System;

namespace TJSettings
{
  [Serializable]
  public class Folder
  {
    public long IdFolder { get; set; }

    public string NameFolder { get; set; }

    public string FullPath { get; set; }

    public int Level { get; set; }

    public Folder(long idFolder, string nameFolder, string fullPath, int level)
    {
      IdFolder = idFolder; NameFolder = nameFolder; FullPath = fullPath; Level = level;
    }

    public static Folder Create(long idFolder, string nameFolder, string fullPath, int level)
    {
      return new Folder(idFolder, nameFolder, fullPath, level);
    }

    public override string ToString()
    {
      return $"{IdFolder}; {NameFolder}; {FullPath}; {Level};";
    }
  }
}