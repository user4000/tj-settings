﻿using System;
using System.Drawing;
using Newtonsoft.Json;

namespace TJSettings
{
  [Serializable]
  public class Notification
  {
    public const int NoValue = -1;

    public TypeEvent TypeOfEvent { get;  set; } = TypeEvent.Unknown;

    public long IdFolder { get;  set; } = NoValue;

    public long IdParent { get;  set; } = NoValue;

    public string NameFolder { get;  set; } = string.Empty;

    public string FolderPath { get; set; } = string.Empty;

    public string IdSetting { get;  set; } = string.Empty;

    public int IdTypeOfSetting { get;  set; } = NoValue;

    public string Value { get;  set; } = string.Empty;

    [JsonIgnore]
    public object Initiator { get; set; } = null;

    protected Notification()
    {

    }

    internal static Notification FolderInsert(object initiator, long idParent, long idFolder, string nameFolder)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.FolderInsert,
        IdParent = idParent,
        IdFolder = idFolder,
        NameFolder = nameFolder
      };
    }

    internal static Notification FolderRename(object initiator, long idFolder, string nameFolder)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.FolderRename,
        IdFolder = idFolder,
        NameFolder = nameFolder
      };
    }

    internal static Notification FolderDelete(object initiator, long idFolder, string nameFolder)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.FolderDelete,
        IdFolder = idFolder,
        NameFolder = nameFolder
      };
    }

    internal static Notification FolderForceDelete(object initiator, string folderPath)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.FolderForceDelete,
        FolderPath = folderPath
      };
    }

    internal static Notification SettingCreateOrUpdate(bool IsCreate, object initiator, long idFolder, string idSetting, int idType, string value)
    {
      return IsCreate ? 
        SettingCreate(initiator, idFolder, idSetting, idType, value) : 
        SettingUpdate(initiator, idFolder, idSetting, idType, value) ;
    }

    internal static Notification SettingCreate(object initiator, long idFolder, string idSetting, int idType, string value)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.SettingCreate,
        IdFolder = idFolder,
        IdSetting = idSetting,
        IdTypeOfSetting = idType,
        Value = value
      };
    }

    internal static Notification SettingUpdate(object initiator, long idFolder, string idSetting, int idType, string value)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.SettingUpdate,
        IdFolder = idFolder,
        IdSetting = idSetting,
        IdTypeOfSetting = idType,
        Value = value
      };
    }

    internal static Notification SaveSetting(object initiator, string folderPath, string idSetting, TypeSetting type, string value)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.SettingCreateOrUpdate,
        FolderPath = folderPath,
        IdSetting = idSetting,
        IdTypeOfSetting = (int)type,
        Value = value
      };
    }

    internal static Notification SettingRename(object initiator, long idFolder, string idSettingOld, string idSettingNew)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.SettingRename,
        IdFolder = idFolder,
        IdSetting = idSettingOld,
        Value = idSettingNew
      };
    }

    internal static Notification SettingDelete(object initiator, long idFolder, string idSetting)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.SettingDelete,
        IdFolder = idFolder,
        IdSetting = idSetting
      };
    }

    internal static Notification DeleteAllSettingsOfOneFolder(object initiator, long idFolder)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.DeleteAllSettingsOfOneFolder,
        IdFolder = idFolder
      };
    }

    internal static Notification SwapRank(object initiator, long idFolder)
    {
      return new Notification()
      {
        Initiator = initiator,
        TypeOfEvent = TypeEvent.SwapRankOfTwoSettings,
        IdFolder = idFolder
      };
    }
  }
}

