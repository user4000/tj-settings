﻿namespace TJSettings
{
  public enum TypeEvent
  {
    Unknown = 0,
    FolderInsert = 1,
    FolderRename = 2,
    FolderDelete = 3,
    FolderForceDelete = 4,
    SettingCreate = 5,
    SettingUpdate = 6,
    SettingCreateOrUpdate = 7,
    SettingDelete = 8,
    SettingRename = 9,   
    DeleteAllSettingsOfOneFolder = 10,
    SwapRankOfTwoSettings = 11
  }
}

