﻿using System;
using TJStandard;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using Telerik.WinControls.UI;
using System.Collections.Generic;

namespace TJSettings
{
  public class ManagerOfLocalDatabase 
  {
    private object User { get; } = null;

    private LocalDatabaseOfSettings DbOperator { get; } = LocalDatabaseFactory.Create(); // This is singleton //

    public EventRegistrator EvRegistrator { get; } = EventRegistratorFactory.Create(); // This is singleton //

    public string RootFolderName { get => DbOperator.RootFolderName; }

    public string FolderPathSeparator { get => DbOperator.FolderPathSeparator; }

    public Converter CvManager { get => DbOperator.CvManager; }

    public string ConnectionString { get => DbOperator.ConnectionString; }

    private ManagerOfLocalDatabase(object user, bool SubscribeToEventsOfLocalSettingsDatabase, string name = "", bool NotifyEvenInCaseOfError = false)
    {
      User = user;
      if (SubscribeToEventsOfLocalSettingsDatabase) Register(user, name, NotifyEvenInCaseOfError);
    }

    public static ManagerOfLocalDatabase Create(object user, bool SubscribeToEventsOfLocalSettingsDatabase, string name = "", bool NotifyEvenInCaseOfError = false)
    {
      /*
      Логика следующая:
      Один экземпляр класса [ManagerOfLocalDatabase] получает ProjectManager
      другой экземпляр класса [ManagerOfLocalDatabase] получает MaintainerFormTreeView.
      Каждый из экземпляров класса [ManagerOfLocalDatabase] имеет члены-синглтоны DbOperator и EvRegistrator.
      Каждый такой синглтон указывает ровно на один и тот же экземпляр своего типа.
      Каждый пользователь класса [ManagerOfLocalDatabase] должен реализовать интерфейс [ILocalDatabaseOfSettingsEventNotification]
      иначе не сможет получать оповещения об изменениях в локальной базе данных.
      */
      return new ManagerOfLocalDatabase(user, SubscribeToEventsOfLocalSettingsDatabase, name, NotifyEvenInCaseOfError);
    }

    public bool Register(object subscriber, string Name, bool NotifyEvenInCaseOfError)
    {
      bool registered = EvRegistrator.Register(subscriber, Name, NotifyEvenInCaseOfError);
      //Trace.WriteLine($"Register {subscriber} -- {Name} -- {NotifyEvenInCaseOfError} -- REGISTERED = {registered}");
      return registered;
    }

    public bool CancelRegistration(object subscriber) => EvRegistrator.CancelRegistration(subscriber);

    public ReturnCode ConnectToDatabase(string PathToDatabase, string ConnectionConfig) => DbOperator.ConnectToDatabase(PathToDatabase, ConnectionConfig);

    public ReturnCode CreateNewDatabase(string text) => DbOperator.CreateNewDatabase(text);

    public string AddRootFolderNameIfNotSpecified(string FullPath) => DbOperator.AddRootFolderNameIfNotSpecified(FullPath);

    #region Operations with FOLDERS ---------------------------------------------------------------------------------------------------

    public ReturnCode FolderInsert(string Parent, string NameFolder)
    {
      ReturnCode code = DbOperator.FolderInsert(Parent, NameFolder);
      return EvRegistrator.EventNotification(code, Notification.FolderInsert(User, CxConvert.ToInt32(code.Note, -1), code.IdObject, NameFolder));
    }

    public ReturnCode FolderInsert(long IdParent, string NameFolder)
    {
      ReturnCode code = DbOperator.FolderInsert(IdParent, NameFolder);
      return EvRegistrator.EventNotification(code, Notification.FolderInsert(User, IdParent, code.IdObject, NameFolder));
    }

    public ReturnCode FolderRename(string FolderPath, string NameFolder)
    {
      ReturnCode code = DbOperator.FolderRename(FolderPath, NameFolder);
      return EvRegistrator.EventNotification(code, Notification.FolderRename(User, code.IdObject, NameFolder));
    }

    public ReturnCode FolderRename(long IdFolder, string NameFolder)
    {
      ReturnCode code = DbOperator.FolderRename(IdFolder, NameFolder);
      return EvRegistrator.EventNotification(code, Notification.FolderRename(User, IdFolder, NameFolder));
    }

    public ReturnCode FolderDelete(string FolderPath, string NameFolder)
    {
      ReturnCode code = DbOperator.FolderDelete(FolderPath, NameFolder);
      return EvRegistrator.EventNotification(code, Notification.FolderDelete(User, code.IdObject, NameFolder));
    }

    public ReturnCode FolderDelete(long IdFolder, string NameFolder)
    {
      ReturnCode code = DbOperator.FolderDelete(IdFolder, NameFolder);
      return EvRegistrator.EventNotification(code, Notification.FolderDelete(User, IdFolder, NameFolder));
    }

    public ReturnCode FolderForceDeleteUsingTreeview(string FolderPath)
    {
      ReturnCode code = DbOperator.FolderForceDeleteUsingTreeview(FolderPath);
      return EvRegistrator.EventNotification(code, Notification.FolderForceDelete(User, FolderPath));
    }

    public ReturnCode FolderForceDelete(string FolderPath)
    {
      ReturnCode code = DbOperator.FolderForceDelete(FolderPath);
      return EvRegistrator.EventNotification(code, Notification.FolderForceDelete(User, FolderPath));
    }

    public async Task<ReturnCode> FolderForceDeleteAsync(string FolderPath)
    {
      ReturnCode code = await DbOperator.FolderForceDeleteAsync(FolderPath);
      return EvRegistrator.EventNotification(code, Notification.FolderForceDelete(User, FolderPath));
    }

    #endregion -----------------------------------------------------------------------------------------------------------------------

    #region Get information about FOLDERS ---------------------------------------------------------------------------------------------------

    public DataTable GetTable(string TableName) => DbOperator.GetTable(TableName);

    public DataTable GetTableFolders() => DbOperator.GetTableFolders();

    public bool FolderExists(string FullPath) => DbOperator.FolderExists(FullPath);

    public string GetRootFolderName() => DbOperator.GetRootFolderName();

    public long GetIdFolder(RadTreeNode node) => DbOperator.GetIdFolder(node);

    public string GetFullPath(long IdFolder) => DbOperator.GetFullPath(IdFolder);

    public long GetIdParent(long IdFolder) => DbOperator.GetIdParent(IdFolder);

    public long GetIdFolder(string FullPath) => DbOperator.GetIdFolder(FullPath);

    public async Task<long> GetIdFolderAsync(string FullPath) => await DbOperator.GetIdFolderAsync(FullPath);

    public List<Folder> GetListOfFolders() => DbOperator.GetListOfFolders();

    public List<string> GetListOfFolders(string ParentFolderPath) => DbOperator.GetListOfFolders(ParentFolderPath);

    public async Task<List<int>> GetListOfIdFoldersAsync(long IdFolder) => await DbOperator.GetListOfIdFoldersAsync(IdFolder);

    public List<int> GetListOfIdFolders(long IdFolder) => DbOperator.GetListOfIdFolders(IdFolder);

    #endregion -----------------------------------------------------------------------------------------------------------------------

    #region Get random data for test ---------------------------------------------------------------------------------------------------

    public long GetRandomIdFolder() => DbOperator.GetRandomIdFolder();

    public string GetRandomIdSetting(long IdFolder) => DbOperator.GetRandomIdSetting(IdFolder);

    public ReturnCode GetRandomSetting() => DbOperator.GetRandomSetting();

    #endregion -----------------------------------------------------------------------------------------------------------------------

    #region Save SETTINGS ---------------------------------------------------------------------------------------------------

    public bool IsCreateSetting(ReturnCode code) => code.Message == TypeEvent.SettingCreate.ToString(); // Check if "CREATE" or "UPDATE" operation //

    public ReturnCode SaveSettingDatetime(bool AddNewSetting, string FolderPath, string IdSetting, DateTime value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SaveSettingDatetime(AddNewSetting, IdFolder, IdSetting, value);
    }

    public ReturnCode SaveSettingDatetime(bool AddNewSetting, long IdFolder, string IdSetting, DateTime value)
    {
      ReturnCode code = DbOperator.SaveSettingDatetime(AddNewSetting, IdFolder, IdSetting, value);     
      return
      AddNewSetting ?
      EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, (int)TypeSetting.Datetime, CvManager.CvDatetime.ToString(value))) :
      EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, (int)TypeSetting.Datetime, CvManager.CvDatetime.ToString(value)));
    }

    public ReturnCode SaveSettingDatetime(string FolderPath, string IdSetting, DateTime value)
    {
      ReturnCode code = DbOperator.SaveSettingDatetime(FolderPath, IdSetting, value);
      return EvRegistrator.EventNotification(code,
        Notification.SettingCreateOrUpdate(IsCreateSetting(code),
          User, code.IdObject, IdSetting, (int)TypeSetting.Datetime, CvManager.CvDatetime.ToString(value)));
    }

    public ReturnCode SaveSettingBoolean(bool AddNewSetting, string FolderPath, string IdSetting, bool value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SaveSettingBoolean(AddNewSetting, IdFolder, IdSetting, value);
    }

    public ReturnCode SaveSettingBoolean(bool AddNewSetting, long IdFolder, string IdSetting, bool value)
    {
      ReturnCode code = DbOperator.SaveSettingBoolean(AddNewSetting, IdFolder, IdSetting, value);
      return
      AddNewSetting ?
      EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, (int)TypeSetting.Boolean, value.ToString())) :
      EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, (int)TypeSetting.Boolean, value.ToString()));
    }

    public ReturnCode SaveSettingBoolean(string FolderPath, string IdSetting, bool value)
    {
      ReturnCode code = DbOperator.SaveSettingBoolean(FolderPath, IdSetting, value);
      return EvRegistrator.EventNotification(code,
        Notification.SettingCreateOrUpdate(IsCreateSetting(code),
          User, code.IdObject, IdSetting, (int)TypeSetting.Boolean, CvManager.CvBoolean.ToString(value)));
    }

    public ReturnCode SaveSettingInteger64(bool AddNewSetting, string FolderPath, string IdSetting, long value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SaveSettingInteger64(AddNewSetting, IdFolder, IdSetting, value);
    }

    public ReturnCode SaveSettingInteger64(bool AddNewSetting, long IdFolder, string IdSetting, long value)
    {
      ReturnCode code = DbOperator.SaveSettingInteger64(AddNewSetting, IdFolder, IdSetting, value);
      return
      AddNewSetting ?
      EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, (int)TypeSetting.Integer64, value.ToString())) :
      EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, (int)TypeSetting.Integer64, value.ToString()));
    }

    public ReturnCode SaveSettingInteger64(string FolderPath, string IdSetting, long value)
    {
      ReturnCode code = DbOperator.SaveSettingInteger64(FolderPath, IdSetting, value);
      return EvRegistrator.EventNotification(code,
        Notification.SettingCreateOrUpdate(IsCreateSetting(code),
          User, code.IdObject, IdSetting, (int)TypeSetting.Integer64, CvManager.CvInt64.ToString(value)));
    }

    public ReturnCode SaveSettingFont(bool AddNewSetting, string FolderPath, string IdSetting, Font value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SaveSettingFont(AddNewSetting, IdFolder, IdSetting, value);
    }

    public ReturnCode SaveSettingFont(bool AddNewSetting, long IdFolder, string IdSetting, Font value)
    {
      ReturnCode code = DbOperator.SaveSettingFont(AddNewSetting, IdFolder, IdSetting, value);
      return
      AddNewSetting ?
      EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, (int)TypeSetting.Font, value.ToString())) :
      EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, (int)TypeSetting.Font, value.ToString()));
    }

    public ReturnCode SaveSettingFont(string FolderPath, string IdSetting, Font value)
    {
      ReturnCode code = DbOperator.SaveSettingFont(FolderPath, IdSetting, value);
      return EvRegistrator.EventNotification(code,
        Notification.SettingCreateOrUpdate(IsCreateSetting(code),
          User, code.IdObject, IdSetting, (int)TypeSetting.Font, CvManager.CvFont.ToString(value)));
    }

    public ReturnCode SaveSettingColor(bool AddNewSetting, string FolderPath, string IdSetting, Color value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SaveSettingColor(AddNewSetting, IdFolder, IdSetting, value);
    }

    public ReturnCode SaveSettingColor(bool AddNewSetting, long IdFolder, string IdSetting, Color value)
    {
      ReturnCode code = DbOperator.SaveSettingColor(AddNewSetting, IdFolder, IdSetting, value);
      return
      AddNewSetting ?
      EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, (int)TypeSetting.Color, value.ToString())) :
      EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, (int)TypeSetting.Color, value.ToString()));
    }

    public ReturnCode SaveSettingColor(string FolderPath, string IdSetting, Color value)
    {
      ReturnCode code = DbOperator.SaveSettingColor(FolderPath, IdSetting, value);
      return EvRegistrator.EventNotification(code,
        Notification.SettingCreateOrUpdate(IsCreateSetting(code),
          User, code.IdObject, IdSetting, (int)TypeSetting.Color, CvManager.CvColor.ToString(value)));
    }

    public ReturnCode SaveSettingText(bool AddNewSetting, string FolderPath, string IdSetting, TypeSetting type, string value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SaveSettingText(AddNewSetting, IdFolder, IdSetting, type, value);
    }

    public ReturnCode SaveSettingText(bool AddNewSetting, long IdFolder, string IdSetting, TypeSetting type, string value)
    {
      ReturnCode code = DbOperator.SaveSettingText(AddNewSetting, IdFolder, IdSetting, type, value);
      return
      AddNewSetting ?
      EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, (int)type, value.ToString())) :
      EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, (int)type, value.ToString()));
    }

    public ReturnCode SaveSettingText(string FolderPath, string IdSetting, TypeSetting type, string value)
    {
      ReturnCode code = DbOperator.SaveSettingText(FolderPath, IdSetting, type, value);
      return EvRegistrator.EventNotification(code,
        Notification.SettingCreateOrUpdate(IsCreateSetting(code),
          User, code.IdObject, IdSetting, (int)TypeSetting.Text, value));
    }

    #endregion -----------------------------------------------------------------------------------------------------------------------

    #region Operation with SETTINGS - these are not for user -------------------------------------------------------------------------

    public ReturnCode SettingCreate(string FolderPath, string IdSetting, int IdType, string value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SettingCreate(IdFolder, IdSetting, IdType, value);
    }

    public ReturnCode SettingCreate(long IdFolder, string IdSetting, int IdType, string value)
    {
      ReturnCode code = DbOperator.SettingCreate(IdFolder, IdSetting, IdType, value);
      return EvRegistrator.EventNotification(code, Notification.SettingCreate(User, IdFolder, IdSetting, IdType, value));
    }

    public ReturnCode SettingUpdate(string FolderPath, string IdSetting, int IdType, string value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SettingUpdate(IdFolder, IdSetting, IdType, value);
    }

    public ReturnCode SettingUpdate(long IdFolder, string IdSetting, int IdType, string value)
    {
      ReturnCode code = DbOperator.SettingUpdate(IdFolder, IdSetting, IdType, value);
      return EvRegistrator.EventNotification(code, Notification.SettingUpdate(User, IdFolder, IdSetting, IdType, value));
    }

    public ReturnCode SettingRename(string FolderPath, string IdSettingOld, string IdSettingNew)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SettingRename(IdFolder, IdSettingOld, IdSettingNew);
    }

    public ReturnCode SettingRename(long IdFolder, string IdSettingOld, string IdSettingNew)
    {
      ReturnCode code = DbOperator.SettingRename(IdFolder, IdSettingOld, IdSettingNew);
      return EvRegistrator.EventNotification(code, Notification.SettingRename(User, IdFolder, IdSettingOld, IdSettingNew));
    }

    public ReturnCode SettingDelete(string FolderPath, string IdSetting)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return SettingDelete(IdFolder, IdSetting);
    }

    public ReturnCode SettingDelete(long IdFolder, string IdSetting)
    {
      ReturnCode code = DbOperator.SettingDelete(IdFolder, IdSetting);
      return EvRegistrator.EventNotification(code, Notification.SettingDelete(User, IdFolder, IdSetting));
    }

    public ReturnCode DeleteAllSettingsOfOneFolder(string FolderPath)
    {
      long IdFolder = GetIdFolder(FolderPath);
      return DeleteAllSettingsOfOneFolder(IdFolder);
    }

    public ReturnCode DeleteAllSettingsOfOneFolder(long IdFolder)
    {
      ReturnCode code = DbOperator.DeleteAllSettingsOfOneFolder(IdFolder);
      return EvRegistrator.EventNotification(code, Notification.DeleteAllSettingsOfOneFolder(User, IdFolder));
    }

    public ReturnCode SwapRank(long IdFolder, Setting settingOne, Setting settingTwo)
    {
      ReturnCode code = DbOperator.SwapRank(IdFolder, settingOne, settingTwo);
      return EvRegistrator.EventNotification(code, Notification.SwapRank(User, IdFolder));
    }

    #endregion -----------------------------------------------------------------------------------------------------------------------

    #region Get information about SETTINGS ---------------------------------------------------------------------------------------------------

    public async Task<BindingList<Setting>> GetSettingsAsync(long IdFolder) => await DbOperator.GetSettingsAsync(IdFolder);

    public List<string> GetSettings(string FullPath, TypeSetting type) => DbOperator.GetSettings(FullPath, type);

    public ReceivedValueText GetStringValueOfSetting(string FolderPath, string IdSetting) => DbOperator.GetStringValueOfSetting(FolderPath, IdSetting);

    public ReceivedValueBoolean GetSettingBoolean(string FolderPath, string IdSetting) => DbOperator.GetSettingBoolean(FolderPath, IdSetting);

    public ReceivedValueInteger64 GetSettingInteger64(string FolderPath, string IdSetting) => DbOperator.GetSettingInteger64(FolderPath, IdSetting);

    public ReceivedValueColor GetSettingColor(string FolderPath, string IdSetting) => DbOperator.GetSettingColor(FolderPath, IdSetting);

    public ReceivedValueFont GetSettingFont(string FolderPath, string IdSetting) => DbOperator.GetSettingFont(FolderPath, IdSetting);

    public ReceivedValueDatetime GetSettingDatetime(string FolderPath, string IdSetting) => DbOperator.GetSettingDatetime(FolderPath, IdSetting);

    public ReceivedValueText GetSettingText(string FolderPath, string IdSetting) => DbOperator.GetSettingText(FolderPath, IdSetting);

    public List<Setting> GetAllSettings() => DbOperator.GetAllSettings();

    #endregion -----------------------------------------------------------------------------------------------------------------------

    public void FillDropDownListForTableTypes(RadDropDownList combobox) => DbOperator.FillDropDownListForTableTypes(combobox);

    public void FillTreeView(RadTreeView treeView, DataTable table) => DbOperator.FillTreeView(treeView, table);

  }
}

