﻿using System.Data.SQLite;
using System.Windows.Forms;
using TJStandard;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    internal string NewSettingValue(int IdType, string value)
    {
      string result = string.Empty;
      TypeSetting type = (TypeSetting)IdType;
      switch (type)
      {
        case TypeSetting.Boolean:
        case TypeSetting.Datetime:
        case TypeSetting.Font:
        case TypeSetting.Color:
        case TypeSetting.Integer64: result = $"; value = {value}"; break;
        case TypeSetting.Password:
        case TypeSetting.File:
        case TypeSetting.Folder:
        case TypeSetting.Text:
          if ((value.Length > 0) && (value.Length < 50)) result = $"; value = {value}";
          break;
        default: break;
      }
      return result;
    }

    internal ReturnCode SettingCreate(string FolderPath, string IdSetting, int IdType, string value)
    {
      return SettingCreate(GetIdFolder(FolderPath), IdSetting, IdType, value);
    }

    internal ReturnCode SettingCreate(long IdFolder, string IdSetting, int IdType, string value)
    {
      //IdSetting = IdSetting.RemoveSpecialCharacters();
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"New setting has been created: {IdSetting}" + NewSettingValue(IdType, value));
      code.IdObject = IdFolder;
      if (IdSetting.Trim().Length < 1) return ReturnCodeFactory.Error((int)Errors.SettingNameNotSpecified, "Setting name not specified");
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingCount))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSetting).ZzGetScalarInteger();
        if (count > 0) return ReturnCodeFactory.Error((int)Errors.SettingAlreadyExists, "A setting with the same name already exists");
        count = command.ZzAdd("@IdType", IdType).ZzAdd("@SettingValue", value).ZzExecuteNonQuery(DbEngine.SqlSettingInsert);
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.SettingInsertFailed, "Error trying to add a new setting");
        code.Note = value;
      }
      return code;
    }

    internal ReturnCode SettingUpdate(string FolderPath, string IdSetting, int IdType, string value)
    {
      return SettingUpdate(GetIdFolder(FolderPath), IdSetting, IdType, value);
    }

    internal ReturnCode SettingUpdate(long IdFolder, string IdSetting, int IdType, string value)
    {
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Changes saved: {IdSetting}" + NewSettingValue(IdType, value));
      code.IdObject = IdFolder;
      if (IdSetting.Trim().Length < 1) return ReturnCodeFactory.Error((int)Errors.SettingNameNotSpecified, "Setting name not specified");
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingCount))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSetting).ZzGetScalarInteger();
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.SettingDoesNotExist, "The setting with the specified name does not exist");
        count = command.ZzAdd("@IdType", IdType).ZzAdd("@SettingValue", value).ZzExecuteNonQuery(DbEngine.SqlSettingUpdate);
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.SettingUpdateFailed, "Error trying to change a value of the setting");
        code.Note = value;
      }
      return code;
    }

    private ReturnCode SettingCreateOrUpdate(bool AddNewSetting, long IdFolder, string IdSetting, TypeSetting type, string value)
    {
      return AddNewSetting ? SettingCreate(IdFolder, IdSetting, (int)(type), value) : SettingUpdate(IdFolder, IdSetting, (int)(type), value);
    }

    private ReturnCode SettingCreateOrUpdate(string FolderPath, string IdSetting, TypeSetting type, string value)
    {
      long IdFolder = GetIdFolder(FolderPath);
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      int IdType = (int)type;
      //IdSetting = IdSetting.RemoveSpecialCharacters();
      ReturnCode code = ReturnCodeFactory.Success($"New setting has been created: {IdSetting}");
      code.IdObject = IdFolder;
      code.Message = string.Empty;
      code.Note = value;

      if (IdSetting.Trim().Length < 1) return ReturnCodeFactory.Error((int)Errors.SettingNameNotSpecified, "Setting name not specified");
      int count = 0;

      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingCount))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSetting).ZzGetScalarInteger();
        if (count > 0) // UPDATE value of existing setting // 
        {
          count = command.ZzAdd("@IdType", IdType).ZzAdd("@SettingValue", value).ZzExecuteNonQuery(DbEngine.SqlSettingUpdate);
          code.Message = TypeEvent.SettingUpdate.ToString();
          if (count != 1)
          {
            code.Number = (int)Errors.SettingUpdateFailed;
            code.Note = "Error trying to change a value of the setting";
            return code;
          }
        }
        else // ADD a new value of setting // 
        {
          count = command.ZzAdd("@IdType", IdType).ZzAdd("@SettingValue", value).ZzExecuteNonQuery(DbEngine.SqlSettingInsert);
          code.Message = TypeEvent.SettingCreate.ToString();
          if (count != 1)
          {
            code.Number = (int)Errors.SettingInsertFailed;
            code.Note = "Error trying to add a new setting";
            return code;
          }
        }
      }
      return code;
    }

    public ReturnCode SettingRename(string FolderPath, string IdSettingOld, string IdSettingNew)
    {      
      return SettingRename(GetIdFolder(FolderPath), IdSettingOld, IdSettingNew);
    }

    public ReturnCode SettingRename(long IdFolder, string IdSettingOld, string IdSettingNew)
    {     
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Setting has been renamed: {IdSettingNew}");
      code.IdObject = IdFolder;
      if (IdSettingNew.Trim().Length < 1) return ReturnCodeFactory.Error((int)Errors.SettingNameNotSpecified, "New setting name not specified");

      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingCount))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSettingNew).ZzGetScalarInteger();
        if (count > 0) return ReturnCodeFactory.Error((int)Errors.SettingAlreadyExists, "A setting with the same name already exists");
        command.Parameters.Clear();
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSettingOld", IdSettingOld).ZzAdd("@IdSettingNew", IdSettingNew).ZzExecuteNonQuery(DbEngine.SqlSettingRename);
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to rename a setting");
      }
      return code;
    }

    public ReturnCode SettingDelete(string FolderPath, string IdSetting)
    {
      return SettingDelete(GetIdFolder(FolderPath), IdSetting);
    }

    public ReturnCode SettingDelete(long IdFolder, string IdSetting)
    {
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Setting has been deleted: {IdSetting}");
      if (IdSetting.Trim().Length < 1) return ReturnCodeFactory.Error((int)Errors.SettingNameNotSpecified, "Setting name not specified");
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingCount))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSetting).ZzGetScalarInteger();
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.SettingNotFound, "Setting not found");
        count = command.ZzExecuteNonQuery(DbEngine.SqlSettingDelete);
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to delete a setting");
      }
      return code;
    }

    public ReturnCode DeleteAllSettingsOfOneFolder(string FolderPath) => DeleteAllSettingsOfOneFolder(GetIdFolder(FolderPath));

    public ReturnCode DeleteAllSettingsOfOneFolder(long IdFolder)
    {
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Settings has been deleted");
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlDeleteAllSettingsOfOneFolder))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ExecuteNonQuery();
        if (count == 0) code = ReturnCodeFactory.Success($"No any settings were deleted");
        count = command.ZzGetScalarInteger(DbEngine.SqlAllSettingsCount);
        if (count != 0) return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to delete all settings of the folder");
      }
      return code;
    }

    public ReturnCode SwapRank(long IdFolder, Setting settingOne, Setting settingTwo)
    {
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Setting rank exchange completed");
      if ((settingOne == null) || (settingTwo == null)) return ReturnCodeFactory.Error("At least one parameter is null");
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSetRank))
      {
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", settingOne.IdSetting).ZzAdd("@Rank", settingTwo.Rank).ExecuteNonQuery();
        if (count != 1) return ReturnCodeFactory.Error("Error trying to change a rank of a setting");
        command.Parameters.Clear();
        count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", settingTwo.IdSetting).ZzAdd("@Rank", settingOne.Rank).ZzExecuteNonQuery(DbEngine.SqlSetRank);
        if (count != 1) return ReturnCodeFactory.Error("Error trying to change a rank of a setting");
      }
      return code;
    }
  }
}

