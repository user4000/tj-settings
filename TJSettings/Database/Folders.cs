﻿using System;
using System.IO;
using TJStandard;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using Telerik.WinControls.UI;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    public string GetRootFolderName() => GetScalarString(DbEngine.SqlGetRootFolderName);

    public bool FolderNotFound(long IdFolder) => IdFolder < 0;

    public ReturnCode FolderNotFound() => ReturnCodeFactory.Error((int)Errors.FolderNotFound, "Folder not found");

    public ReturnCode FolderInsert(string Parent, string NameFolder) => FolderInsert(GetIdFolder(Parent), NameFolder);

    public ReturnCode FolderInsert(long IdParent, string NameFolder)
    {
      if (FolderNotFound(IdParent)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"New folder has been created: {NameFolder}");
      code.Note = IdParent.ToString();
      int IdNewFolder = -1;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlFolderCountByIdParent))
      {
        command.ZzAdd("@IdParent", IdParent).ZzAdd("@NameFolder", NameFolder);
        int count = command.ZzGetScalarInteger();
        if (count != 0) return ReturnCodeFactory.Error((int)Errors.FolderAlreadyExists, "A folder with the same name already exists");
        count = command.ZzExecuteNonQuery(DbEngine.SqlFolderInsert);
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to add a new folder");
        IdNewFolder = command.ZzGetScalarInteger(DbEngine.SqlGetIdFolder);
        if (IdNewFolder > 0)
        {
          code.IdObject = IdNewFolder;
        }
        else
        {
          return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to add a new folder");
        }
      }
      return code;
    }

    public ReturnCode FolderRename(string FolderPath, string NameFolder) => FolderRename(GetIdFolder(FolderPath), NameFolder);

    public ReturnCode FolderRename(long IdFolder, string NameFolder)
    {
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Folder has been renamed: {NameFolder}");
      code.IdObject = IdFolder;
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlFolderCountByIdFolder))
      {
        command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@NameFolder", NameFolder);
        count = command.ZzGetScalarInteger();
        if (count != 0) return ReturnCodeFactory.Error((int)Errors.FolderAlreadyExists, "A folder with the same name already exists");
        count = command.ZzExecuteNonQuery(DbEngine.SqlFolderRename);
        if (count < 1) return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to rename a folder");
      }
      if (IdFolder == DbEngine.IdFolderRoot) RootFolderName = GetRootFolderName();
      return code;
    }

    public ReturnCode FolderDelete(string FolderPath, string NameFolder) => FolderDelete(GetIdFolder(FolderPath), NameFolder);

    public ReturnCode FolderDelete(long IdFolder, string NameFolder)
    {
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      ReturnCode code = ReturnCodeFactory.Success($"Folder has been deleted: {NameFolder}");
      code.IdObject = IdFolder;
      int count = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlFolderCountSimple))
      {
        command.ZzAdd("@IdFolder", IdFolder);
        count = command.ZzGetScalarInteger();
        if (count != 1) return ReturnCodeFactory.Error((int)Errors.FolderNotFound, "The folder you specified was not found");
        count = command.ZzGetScalarInteger(DbEngine.SqlCountChildFolder);
        if (count != 0) return ReturnCodeFactory.Error((int)Errors.FolderHasChildFolder, "You cannot delete a folder that has other folders inside");
        count = command.ZzGetScalarInteger(DbEngine.SqlCountChildSettings);
        if (count != 0) return ReturnCodeFactory.Error((int)Errors.FolderHasSettings, "You cannot delete a folder that contains settings");
        count = command.ZzExecuteNonQuery(DbEngine.SqlFolderDelete);
        if (count == 0) return ReturnCodeFactory.Error((int)Errors.Unknown, "Error trying to delete a folder");
      }
      return code;
    }

    private ReturnCode FolderForceDeleteUsingTreeviewOldVersionDeprecated(string FolderPath)
    {
      // Trace.WriteLine($"FolderForceDelete(string FolderPath)  ---> {FolderPath}");
      // This method uses RadTreeView class to perform search operations //
      ReturnCode code = ReturnCodeFactory.Error();

      FolderPath = AddRootFolderNameIfNotSpecified(FolderPath);

      long IdFolder = GetIdFolder(FolderPath);

      if (FolderNotFound(IdFolder)) return FolderNotFound();

      //Trace.WriteLine($"FolderForceDelete(string FolderPath)  ---> Point 1 --- IdFolder = {IdFolder}");

      bool FlagFound = false;

      int count = 0;

      void SearchNode(RadTreeNode node)
      {
        if (FlagFound) return;
        //if (GetIdFolder(node) == IdFolder)
        if (node.FullPath == FolderPath)
        {
          FlagFound = true;
          //Trace.WriteLine($"Found !!! ---> {node.FullPath}");
          LocalMethodDeleteAllSettingsOfOneFolder(node);
          LocalMethodDeleteFolderAndAllChildFolders(node);
        }
        if (!FlagFound) foreach (var item in node.Nodes) SearchNode(item);
      }

      void LocalMethodDeleteAllSettingsOfOneFolder(RadTreeNode node)
      {
        DeleteAllSettingsOfOneFolder(node.FullPath);
        //Trace.WriteLine($"Delete settings of folder ======= {node.FullPath}");
        foreach (var item in node.Nodes) LocalMethodDeleteAllSettingsOfOneFolder(item);
      }

      void LocalMethodDeleteFolderAndAllChildFolders(RadTreeNode node)
      {
        foreach (var item in node.Nodes) LocalMethodDeleteFolderAndAllChildFolders(item);
        ReturnCode result = FolderDelete(node.FullPath, string.Empty);
        if (IdFolder == GetIdFolder(node)) code = result;
        count++;
        //Trace.WriteLine($@"Delete folder /\/\/\/\/\/\/\ === {node.FullPath}");
      }

      DataTable table = GetTableFolders();
      FxTreeView form = new FxTreeView();
      form.Visible = false;
      FillTreeView(form.TvFolders, table);

      foreach (var item in form.TvFolders.Nodes) SearchNode(item);

      form.TvFolders.DataSource = null;
      table.Clear();
      form.TvFolders.Dispose();
      table.Dispose();
      form.Close();

      //Trace.WriteLine($@" >>>>>>>>>>>>>>>>>>>>> {ReturnCodeFormatter.ToString(code)}");
      code.Note += $" Folders processed = {count}";
      return code;
    }

    public ReturnCode FolderForceDeleteUsingTreeview(string FolderPath)
    {
      // This method uses RadTreeView class to perform search operations //
      ReturnCode code; // = ReturnCodeFactory.Error();
      FolderPath = AddRootFolderNameIfNotSpecified(FolderPath);    
      long IdFolder = GetIdFolder(FolderPath);
      if (FolderNotFound(IdFolder)) return FolderNotFound();
      if (IdFolder == DbEngine.IdFolderRoot) return ReturnCodeFactory.Error("The root folder cannot be specified as an argument to this function.");
      bool FlagFound = false;
      int count = 0;
      Stack<long> stack = new Stack<long>();

      void MarkNodeToBeDeleted(RadTreeNode node)
      {
        stack.Push(GetIdFolder(node));
        //Trace.WriteLine($"Push to stack >>> {node.FullPath} === {x}");
        foreach (var item in node.Nodes) MarkNodeToBeDeleted(item);
      }

      void SearchNode(RadTreeNode node)
      {
        if (FlagFound) return;
        //if (GetIdFolder(node) == IdFolder)
        if (node.FullPath == FolderPath)
        {
          FlagFound = true;
          MarkNodeToBeDeleted(node);
        }
        if (!FlagFound) foreach (var item in node.Nodes) SearchNode(item);
      }

      DataTable table = GetTableFolders();
      FxTreeView form = new FxTreeView();
      form.Visible = false;
      FillTreeView(form.TvFolders, table);

      foreach (var item in form.TvFolders.Nodes) SearchNode(item);

      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand())
        while (stack.Count > 0)
        {
          long x = stack.Pop();          
          command.Parameters.Clear();
          command.ZzAdd("@IdFolder", x).ZzExecuteNonQuery(DbEngine.SqlDeleteAllSettingsOfOneFolder);
          if (stack.Count > 0) command.ZzExecuteNonQuery(DbEngine.SqlFolderDelete);
          count++;
        }

      code = FolderDelete(FolderPath, FolderPath);

      form.TvFolders.DataSource = null;
      table.Clear();
      form.TvFolders.Dispose();
      table.Dispose();
      form.Close();

      code.Note += $" Folders processed = {count}";
      return code;
    }

    public ReturnCode FolderForceDelete(string FolderPath)
    {
      ReturnCode code; // = ReturnCodeFactory.Error();
      long IdFolder = GetIdFolder(FolderPath);

      if (FolderNotFound(IdFolder)) return FolderNotFound();
      if (IdFolder == DbEngine.IdFolderRoot) return ReturnCodeFactory.Error("The root folder cannot be specified as an argument to this function.");
      List<int> list = GetListOfIdFolders(IdFolder);
      int count = 0;

      void ProcessOneFolder(SQLiteCommand cmd, int InnerIdFolder)
      {
        List<int> InnerList = GetListOfIdFolders(cmd, InnerIdFolder);
        foreach (int item in InnerList) ProcessOneFolder(cmd, item);
        DeleteSettingsAndFolder(cmd, InnerIdFolder);
      }

      void DeleteSettingsAndFolder(SQLiteCommand cmd, int InnerIdFolder)
      {
        cmd.Parameters.Clear();
        cmd.ZzAdd("@IdFolder", InnerIdFolder).ZzExecuteNonQuery(DbEngine.SqlDeleteAllSettingsOfOneFolder);
        cmd.ZzExecuteNonQuery(DbEngine.SqlFolderDelete);
        count++;
      }

      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand())
      {
        foreach (int item in list) ProcessOneFolder(command, item);
      }

      DeleteAllSettingsOfOneFolder(IdFolder);
      code = FolderDelete(IdFolder, FolderPath);
      code.Note += $" Folders processed = {++count}";
      return code;
    }

    public async Task<ReturnCode> FolderForceDeleteAsync(string FolderPath)
    {
      ReturnCode code; // = ReturnCodeFactory.Error();
      long IdFolder = await GetIdFolderAsync(FolderPath);

      if (FolderNotFound(IdFolder)) return FolderNotFound();
      if (IdFolder == DbEngine.IdFolderRoot) return ReturnCodeFactory.Error("The root folder cannot be specified as an argument to this function.");
      List<int> list = await GetListOfIdFoldersAsync(IdFolder);
      int count = 0;

      async Task ProcessOneFolder(SQLiteCommand cmd, int InnerIdFolder)
      {
        List<int> InnerList = GetListOfIdFolders(cmd, InnerIdFolder);
        foreach (int item in InnerList) await ProcessOneFolder(cmd, item);
        await DeleteSettingsAndFolder(cmd, InnerIdFolder);
      }

      async Task DeleteSettingsAndFolder(SQLiteCommand cmd, int InnerIdFolder)
      {
        cmd.Parameters.Clear();
        await cmd.ZzAdd("@IdFolder", InnerIdFolder).ZzExecuteNonQueryAsync(DbEngine.SqlDeleteAllSettingsOfOneFolder);
        cmd.ZzExecuteNonQuery(DbEngine.SqlFolderDelete);
        count++;
      }

      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand())
      {
        foreach (int item in list) await ProcessOneFolder(command, item);
      }

      DeleteAllSettingsOfOneFolder(IdFolder);
      code = FolderDelete(IdFolder, FolderPath);
      code.Note += $" Folders processed = {++count}";
      return code;
    }

    public string AddRootFolderNameIfNotSpecified(string FullPath)
    {
      FullPath = FullPath.Trim();
      if (FullPath == string.Empty) return RootFolderName;
      FullPath = FullPath.TrimStart(FolderPathSeparator[0]).TrimEnd(FolderPathSeparator[0]);
      if (FullPath.StartsWith(RootFolderName) == false) FullPath = RootFolderName + FolderPathSeparator + FullPath;
      return FullPath;
    }

    public long GetIdFolder(RadTreeNode node)
    {
      long Id = IdFolderNotFound;
      if (node != null)
        try
        {
          DataRowView row = node.DataBoundItem as DataRowView;
          Id = CxConvert.ToInt64(row.Row[DbEngine.CnFoldersIdFolder].ToString(), IdFolderNotFound);
        }
        catch { }
      return Id;
    }

    /// <summary>
    /// Find IdFolder by Materialized Full Path (Root Folder Name may be omitted).
    /// </summary>

    public bool FolderExists(string FullPath) => FolderNotFound(GetIdFolder(FullPath)) == false;


    /// <summary>
    /// Find Materialized Full Path by IdFolder.
    /// </summary>

    public string GetFullPath(long IdFolder)
    {
      int j = 0;
      RowFolder folder; string json; string path = string.Empty;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetFolderAsJson))
      {
        bool flag = true;
        while (flag)
        {
          json = command.ZzAdd("@IdFolder", IdFolder).ZzGetScalarString();
          folder = CxConvert.JsonToObject<RowFolder>(json);
          if (folder == null) return string.Empty;
          path = folder.NameFolder + this.FolderPathSeparator + path;
          IdFolder = folder.IdParent;
          command.Parameters.Clear();
          if (folder.IdFolder <= 0) flag = false;
          if (j++ > 20) flag = false;
        }
      }
      return path.TrimEnd(this.FolderPathSeparatorCharacter);
    }

    public long GetIdParent(long IdFolder)
    {
      long IdParent = 0;
      string sql = string.Empty;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetIdParent))
      {
        IdParent = command.ZzAdd("@IdFolder", IdFolder).ZzGetScalarInteger();
      }
      return IdParent;
    }

    public long GetIdFolder(string FullPath) // Find IdFolder by Materialized Full Path - Root Folder Name may be omitted //
    {
      FullPath = AddRootFolderNameIfNotSpecified(FullPath);
      string[] names = FullPath.Split(FolderPathSeparator[0]);
      long IdFolder = 0;
      string sql = string.Empty;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetIdFolder))
      {
        for (int i = 0; i < names.Length; i++)
        {
          IdFolder = command.ZzAdd("@IdParent", IdFolder).ZzAdd("@NameFolder", names[i]).ZzGetScalarInteger();
          command.Parameters.Clear();
          if (IdFolder < 0) break;
        }
      }
      return IdFolder;
    }

    public async Task<long> GetIdFolderAsync(string FullPath) // Find IdFolder by Materialized Full Path - Root Folder Name may be omitted //
    {
      FullPath = AddRootFolderNameIfNotSpecified(FullPath);
      string[] names = FullPath.Split(FolderPathSeparator[0]);
      long IdFolder = 0;
      string sql = string.Empty;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetIdFolder))
      {
        for (int i = 0; i < names.Length; i++)
        {
          IdFolder = await command.ZzAdd("@IdParent", IdFolder).ZzAdd("@NameFolder", names[i]).ZzGetScalarIntegerAsync();
          command.Parameters.Clear();
          if (IdFolder < 0) break;
        }
      }
      return IdFolder;
    }

    private long GetIdFolderUsingTreeview(string FullPath)
    {
      long x = IdFolderNotFound;

      long ProcessOneNode(RadTreeNode node)
      {
        if (node.FullPath == FullPath) return GetIdFolder(node);
        foreach (var item in node.Nodes)
        {
          x = ProcessOneNode(item);
          if (x != IdFolderNotFound) { return x; }
        }
        return -1;
      }

      DataTable table = GetTableFolders();
      FxTreeView form = new FxTreeView();
      form.Visible = false;
      FillTreeView(form.TvFolders, table);

      long IdFolder = IdFolderNotFound;

      foreach (var item in form.TvFolders.Nodes)
      {
        IdFolder = ProcessOneNode(item);
        if (IdFolder != IdFolderNotFound) break;
      }

      form.TvFolders.DataSource = null;
      table.Clear();
      form.TvFolders.Dispose();
      table.Dispose();
      form.Close();
      return IdFolder;
    }

    /// <summary>
    /// Get all folders of the database.
    /// </summary>
    public List<Folder> GetListOfFolders()
    {
      List<Folder> list = new List<Folder>();
      void ProcessOneNode(RadTreeNode node)
      {
        list.Add(Folder.Create(GetIdFolder(node), node.Text, node.FullPath, node.Level));
        foreach (var item in node.Nodes) ProcessOneNode(item);
      }
      DataTable table = GetTableFolders();
      FxTreeView form = new FxTreeView();
      form.Visible = false;
      FillTreeView(form.TvFolders, table);
      foreach (var item in form.TvFolders.Nodes) ProcessOneNode(item);
      form.TvFolders.DataSource = null;
      table.Clear();
      form.TvFolders.Dispose();
      table.Dispose();
      form.Close();
      return list;
    }

    /// <summary>
    /// Get names of all direct child folders of the specified folder.
    /// </summary>
    public List<string> GetListOfFolders(string ParentFolderPath)
    {
      List<string> list = new List<string>();
      DataTable table = new DataTable();
      long IdFolder = GetIdFolder(ParentFolderPath);
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlFolderGetChildren).ZzAdd("@IdFolder", IdFolder))
      using (SQLiteDataReader reader = command.ExecuteReader())
      {
        table.Load(reader);
      }
      if (table.Rows.Count > 0) list = (from DataRow row in table.Rows select row[0].ToString()).ToList();
      table.Clear();
      return list;
    }

    public async Task<List<int>> GetListOfIdFoldersAsync(long IdFolder)
    {
      List<int> list = new List<int>();
      int x = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlFolderGetIdChildren).ZzAdd("@IdFolder", IdFolder))
      using (DbDataReader reader = await command.ExecuteReaderAsync())
        while (await reader.ReadAsync())
        {
          x = reader.GetInt32(0);
          if (x > 0) list.Add(x);
        }
      return list;
    }

    public List<int> GetListOfIdFolders(long IdFolder)
    {
      List<int> list = new List<int>();
      int x = 0;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlFolderGetIdChildren).ZzAdd("@IdFolder", IdFolder))
      using (SQLiteDataReader reader = command.ExecuteReader())
        while (reader.Read())
        {
          x = reader.GetInt32(0);
          if (x > 0) list.Add(x);
        }
      return list;
    }

    private List<int> GetListOfIdFolders(SQLiteCommand command, long IdFolder)
    {
      List<int> list = new List<int>();
      int x = 0;
      command.Parameters.Clear();
      using (SQLiteDataReader reader = command.ZzText(DbEngine.SqlFolderGetIdChildren).ZzAdd("@IdFolder", IdFolder).ExecuteReader())
        while (reader.Read())
        {
          x = reader.GetInt32(0);
          if (x > 0) list.Add(x);
        }
      return list;
    }
  }
}

