﻿using System.Data.SQLite;
using TJStandard;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    public int GetRandomIdFolder()
    {
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetRandomIdFolder))
      {
        return command.ZzGetScalarInteger();
      }
    }

    public string GetRandomIdSetting(long IdFolder)
    {
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetRandomIdSetting))
      {
        return command.ZzAdd("@IdFolder", IdFolder).ZzGetScalarString();
      }
    }

    public ReturnCode GetRandomSetting()
    {
      ReturnCode code = ReturnCodeFactory.Error();
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetRandomSetting))
      using (SQLiteDataReader reader = command.ExecuteReader())
        while (reader.Read())
        {
          code = ReturnCodeFactory.Create(ReturnCodeFactory.NcSuccess, reader.GetInt32(0), reader.GetString(1), string.Empty);
          //Trace.WriteLine("---> " + ReturnCodeFormatter.ToString(code));
          break;
        }
      return code;
    }
  }
}

