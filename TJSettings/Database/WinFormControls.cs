﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using TJStandard;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    public void FillDropDownListForTableTypes(RadDropDownList combobox)
    {
      ClearDataSource(combobox.DataSource);
      combobox.DataSource = GetTable(DbEngine.TnTypes); 
      combobox.ValueMember = DbEngine.CnTypesIdType;
      combobox.DisplayMember = DbEngine.CnTypesNameType;
      combobox.ZzSetStandardVisualStyle();
    }

    internal void ClearDataSource(object DataSource)
    {
      if (DataSource is DataTable) { ((DataTable)DataSource).Clear(); }
    }

    public void FillTreeView(RadTreeView treeView, DataTable table)
    {
      IEnumerable<DataRow> BadRows = table.Rows.Cast<DataRow>().Where(r => r[DbEngine.CnFoldersIdFolder].ToString() == r[DbEngine.CnFoldersIdParent].ToString());
      BadRows.ToList().ForEach(r => r.SetField(DbEngine.CnFoldersIdParent, DBNull.Value));
      treeView.DisplayMember = DbEngine.CnFoldersNameFolder;
      treeView.ParentMember = DbEngine.CnFoldersIdParent;
      treeView.ChildMember = DbEngine.CnFoldersIdFolder;
      treeView.DataSource = table; // <== This may cause application crash if there is any row having IdParent==IdFolder
      treeView.SortOrder = SortOrder.Ascending;
    }
  }
}

