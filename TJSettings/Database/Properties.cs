﻿using System.Data;
using Telerik.WinControls.UI;
using TJStandard;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    internal DatabaseStructureManager DbEngine { get; } = DatabaseStructureManager.Create();

    public string RootFolderName { get; private set; } = string.Empty;

    public char SingleQuote { get; } = '\'';

    public long IdFolderNotFound { get; } = -1;

    public string FolderPathSeparator { get; } = @"\";

    public char FolderPathSeparatorCharacter { get; }

    public string DefaultFolder { get; } = "settings";

    public string DefaultFileName { get; } = "application_settings.db";

    public Converter CvManager { get; } = new Converter();

    public string ConnectionString { get; private set; } = string.Empty;

    public string ConnectionConfigDefault { get; } = "Version=3;Pooling=True;Max Pool Size=100;Cache Size=100000;Page Size=1024;";

    internal LocalDatabaseOfSettings()
    {
      FolderPathSeparatorCharacter = FolderPathSeparator.ToCharArray()[0];
    }
  }
}

