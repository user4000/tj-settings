﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Telerik.WinControls.UI;
using TJStandard;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    private async Task<BindingList<Setting>> GetSettings(RadTreeNode node)
    {
      if (node == null) return null;
      return await GetSettingsAsync(GetIdFolder(node));
    }

    public async Task<BindingList<Setting>> GetSettingsAsync(long IdFolder)
    {
      BindingList<Setting> list = new BindingList<Setting>();
      if (FolderNotFound(IdFolder)) return list;

      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingSelect).ZzAdd("@IdFolder", IdFolder))
      {
        using (SQLiteDataReader reader = command.ExecuteReader())
          while (await reader.ReadAsync())
            list.ZzAdd
              (
                IdFolder: reader.GetInt32(0),
                IdSetting: reader.GetString(1),
                IdType: reader.GetInt32(2),
                NameType: reader.GetString(3),
                SettingValue: reader.GetString(4),
                Rank: reader.GetInt32(5),
                BooleanValue: reader.GetString(6)
              );

        if (ListHasDuplicatedRank(list)) // Correct rank if duplicate values occur //
        {
          string sql = DbEngine.SqlSetRank.Replace("@IdFolder", IdFolder.ToString());
          string temp = string.Empty;

          for (int i = 0; i < list.Count; i++)
          {
            list[i].Rank = i + 1;
            temp += sql.Replace("@Rank", list[i].Rank.ToString()).Replace("@IdSetting", SingleQuote + list[i].IdSetting + SingleQuote) + "; \n";
          }
          command.Parameters.Clear();
          command.ZzExecuteNonQuery(temp);
        }
      } // end of [using connection] and [using command] //
      return list;
    }

    /// <summary>
    /// Get list of settings of the specified type. First argument is a Full Path to a Folder, containing the settings.
    /// </summary>
    public List<string> GetSettings(string FullPath, TypeSetting type)
    {
      List<string> list = new List<string>();
      long IdFolder = GetIdFolder(FullPath);
      if (IdFolder < 0) return list;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingSelect).ZzAdd("@IdFolder", IdFolder))
      using (SQLiteDataReader reader = command.ExecuteReader())
        while (reader.Read()) if ((reader.GetInt32(2) == (int)type) || (type == TypeSetting.Unknown)) list.Add(reader.GetString(1));
      return list;
    }

    public ReceivedValueText GetStringValueOfSettingThisIsPreviousVersionOfMethod(string FolderPath, string IdSetting)
    {
      long IdFolder = GetIdFolder(FolderPath);
      if (IdFolder < 0) return ReceivedValueText.Error((int)Errors.FolderNotFound, "Folder not found");
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlSettingCount))
      {
        int count = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSetting).ZzGetScalarInteger();
        if (count == 0) return ReceivedValueText.Error((int)Errors.SettingDoesNotExist, "Setting does not exist");
        string value = command.ZzGetScalarString(DbEngine.SqlGetSettingValue);
        return ReceivedValueText.Success(value);
      }
    }

    public ReceivedValueText GetStringValueOfSetting(string FolderPath, string IdSetting) 
    {
      long IdFolder = GetIdFolder(FolderPath);
      if (IdFolder < 0) return ReceivedValueText.Error((int)Errors.FolderNotFound, "Folder not found");
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetSettingValueWithCount))
      {
        string value = command.ZzAdd("@IdFolder", IdFolder).ZzAdd("@IdSetting", IdSetting).ZzGetScalarString();
        if (value.Length < 1) return ReceivedValueText.Error((int)Errors.Unknown, "Query has returned empty string");
        if (value[value.Length - 1] == '0') return ReceivedValueText.Error((int)Errors.SettingDoesNotExist, "Setting does not exist");
        if (value.Length == 1) return ReceivedValueText.Success(string.Empty);
        return ReceivedValueText.Success(value.Remove(value.Length - 1));
      }
    }

    public ReceivedValueBoolean GetSettingBoolean(string FolderPath, string IdSetting)
    {
      ReceivedValueText TextValue = GetStringValueOfSetting(FolderPath, IdSetting);
      if (TextValue.Code.Error) return ReceivedValueBoolean.Error(TextValue.Code.Number, TextValue.Code.Message);
      return CvManager.CvBoolean.FromString(TextValue.Value);
    }

    public ReceivedValueInteger64 GetSettingInteger64(string FolderPath, string IdSetting)
    {
      ReceivedValueText TextValue = GetStringValueOfSetting(FolderPath, IdSetting);
      if (TextValue.Code.Error) return ReceivedValueInteger64.Error(TextValue.Code.Number, TextValue.Code.Message);
      return CvManager.CvInt64.FromString(TextValue.Value);
    }

    public ReceivedValueColor GetSettingColor(string FolderPath, string IdSetting)
    {
      ReceivedValueText TextValue = GetStringValueOfSetting(FolderPath, IdSetting);
      if (TextValue.Code.Error) return ReceivedValueColor.Error(TextValue.Code.Number, TextValue.Code.Message);
      return CvManager.CvColor.FromString(TextValue.Value);
    }

    public ReceivedValueFont GetSettingFont(string FolderPath, string IdSetting)
    {
      ReceivedValueText TextValue = GetStringValueOfSetting(FolderPath, IdSetting);
      if (TextValue.Code.Error) return ReceivedValueFont.Error(TextValue.Code.Number, TextValue.Code.Message);
      return CvManager.CvFont.FromString(TextValue.Value);
    }

    public ReceivedValueDatetime GetSettingDatetime(string FolderPath, string IdSetting)
    {
      ReceivedValueText TextValue = GetStringValueOfSetting(FolderPath, IdSetting);
      if (TextValue.Code.Error) return ReceivedValueDatetime.Error(TextValue.Code.Number, TextValue.Code.Message);
      return CvManager.CvDatetime.FromString(TextValue.Value);
    }

    public ReceivedValueText GetSettingText(string FolderPath, string IdSetting)
    {
      return GetStringValueOfSetting(FolderPath, IdSetting);
    }

    public ReturnCode CheckIdSettingCharacters(string IdSetting)
    {
      if (IdSetting.Trim() == string.Empty) return ReturnCodeFactory.Error("Incorrect name of the setting.");
      string CorrectedIdSetting = IdSetting.RemoveSpecialCharacters();
      if (CorrectedIdSetting != IdSetting)
        return ReturnCodeFactory.Error($"Special characters are not allowed in the setting name. Allowed name may be = {CorrectedIdSetting}");
      return ReturnCodeFactory.Success();
    }

    /// <summary>
    /// Get all settings of all folders.
    /// </summary>
    public List<Setting> GetAllSettings()
    {
      ReturnCode code = ReturnCodeFactory.Error();
      List<Setting> list = new List<Setting>();
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlGetAllSettings))
      using (SQLiteDataReader reader = command.ExecuteReader())
        while (reader.Read())
          list.Add
            (
              Setting.Create
              (
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetInt32(2),
                reader.GetString(3),
                reader.GetInt32(4)
              )
            );
      return list;
    }

    public bool ListHasDuplicatedRank(BindingList<Setting> list)
    {
      var duplicates = list.GroupBy(x => x.Rank).Where(item => item.Count() > 1);
      int count = 0;
      foreach (var duplicate in duplicates) foreach (var item in duplicate) { count++; break; }
      return count > 0;
    }
  }
}

