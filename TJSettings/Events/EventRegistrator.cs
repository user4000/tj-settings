﻿using System;
using System.Collections.Concurrent;
using TJStandard;

namespace TJSettings
{
  public class EventRegistrator
  {
    private ConcurrentDictionary<object,Tuple<string, bool>> Clients { get; } = new ConcurrentDictionary<object, Tuple<string, bool>>();

    internal EventRegistrator() { }
   
    internal bool Register(object subscriber, string Name, bool NotifyEvenInCaseOfError)
    {           
      if ((subscriber is ILocalDatabaseOfSettingsEventNotification) == false) return false;
      if (Clients.ContainsKey(subscriber)) return true;
      return Clients.TryAdd(subscriber, new Tuple<string, bool>(Name, NotifyEvenInCaseOfError));
    }

    internal bool CancelRegistration(object subscriber)
    {
      if (Clients.ContainsKey(subscriber)) return Clients.TryRemove(subscriber, out Tuple<string, bool> value);
      return true;
    }

    internal ReturnCode EventNotification(ReturnCode code, Notification msg)
    {
      //Trace.WriteLine($"EventNotification {code.Number} -- {code.IdObject} -- {code.Message} -- {code.Note} **** {msg.IdTypeOfSetting.ToString()} -- {msg.IdFolder} -- {msg.IdSetting}");
      foreach (var client in Clients)
        if (code.Success || client.Value.Item2) // Если (выполнение успешно) ИЛИ (не успешно, но клиент подписан также на неуспешные попытки действий) //
         (client.Key as ILocalDatabaseOfSettingsEventNotification).EventNotification(msg, code, client.Key, client.Value.Item1);
      return code;
    }
  }
}

