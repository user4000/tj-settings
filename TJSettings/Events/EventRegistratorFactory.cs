﻿namespace TJSettings
{
  public class EventRegistratorFactory // This is singleton //
  {
    private static readonly object padlock = new object();
    private static EventRegistrator Registrator { get; set; } = null;
    public static EventRegistrator Create()
    {
      if (Registrator == null)
      {
        lock (padlock)
        {
          if (Registrator == null)
          {
            Registrator = new EventRegistrator();
          }
        }
      }
      return Registrator;
    }
  }
}

