﻿using TJStandard;

namespace TJSettings
{
  public interface ILocalDatabaseOfSettingsEventNotification
  {   
    void EventNotification(Notification msg, ReturnCode code, object subscriber, string subscriberName);
  }
}

