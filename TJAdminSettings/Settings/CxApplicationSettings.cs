﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using TJFramework;
using TJSettingsFormTreeView;
using static TJAdminSettings.Program;
using static TJFramework.Logger.Manager;

namespace TJAdminSettings
{
  [Serializable] // This class must be an INHERITOR of the [FormTreeViewApplicationSettings] //
  public class CxApplicationSettings : FormTreeViewApplicationSettings 
  {
    [Category("User interface")]
    [DisplayName("Orientation tabs of the main form")]
    public StripViewAlignment MainPageOrientation { get; set; } = StripViewAlignment.Top;

    [Browsable(false)]
    public override bool AutoHideTreeViewSettingsTab { get; set; } = false;

    public override void PropertyValueChanged(string PropertyName)
    {

      if (PropertyName == nameof(MainPageOrientation))
        TJFrameworkManager.Service.SetMainPageViewOrientation(MainPageOrientation);
    }

    public override void EventBeforeSaving()
    {

    }

    public override void EventAfterSaving()
    {

    }
  }
}



