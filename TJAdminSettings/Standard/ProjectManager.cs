﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJSettings;
using TJSettingsFormTreeView;
using TJStandard;
using static TJFramework.TJFrameworkManager;

namespace TJAdminSettings
{
  public class ProjectManager : IOutputMessage
  {
    public const string Empty = "";

    public ManagerFormTreeView MnFormTreeView { get; internal set; } = null;

    //public LocalDatabaseOfSettings DbSettings { get => MnFormTreeView.DbSettings; }

    public ManagerOfLocalDatabase DbSettings { get => MnFormTreeView.DbSettings; }

    public void OutputMessage(string message, string header = Empty)
    {
      Ms.Message(message, header).NoAlert().Debug();
    }

    internal void EventPageChanged(string name)
    {

    }

    internal void EventAfterAllFormsAreCreated()
    {

    }

    internal void EventBeforeMainFormClose()
    {

    }
  }
}