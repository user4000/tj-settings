﻿using System.Windows.Forms;
using TJFramework;
using TJSettings;
using TJStandard;

namespace TJSettingsFormTreeView
{
  public class ManagerFormTreeViewFactory
  {
    public static ManagerFormTreeView Create(FormTreeViewApplicationSettings settings, FormTreeViewMode mode)
    {
      ManagerFormTreeView manager = new ManagerFormTreeView();
      manager.Settings = settings; // Very important. This is a client application settings //
      manager.OneOrManyDatabases = mode;
      //manager.DbSettings = LocalDatabaseOfSettings.Create();

      bool SubscribeToEventsOfLocalSettingDatabase = mode == FormTreeViewMode.OneLocalSettingsDatabase; // Do not subscribe if [ADMIN MODE] is specified //
      manager.MnSettings = ManagerOfLocalDatabase.Create(manager, SubscribeToEventsOfLocalSettingDatabase, nameof(ManagerFormTreeView), false);

      TJFrameworkManager.Service.EventPageChanged += manager.EventPageChanged;
      TJFrameworkManager.Service.EventAfterAllFormsAreCreated += manager.EventAfterAllFormsAreCreated;
      TJFrameworkManager.Service.EventBeforeMainFormClose += manager.EventBeforeMainFormClose;

      /*   
      No need to instantly connect to the database.

      The user can perform the connection operation later.

      ReturnCode code = manager.DbSettings.ConnectToDatabase(ApplicationSettings.SettingsDatabaseLocation);
      
      if (code.Error)
      {
        MessageBox.Show("Could not connect to the database of settings", code.StringValue + " " + code.StringNote, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      */

      return manager;
    }
  }
}