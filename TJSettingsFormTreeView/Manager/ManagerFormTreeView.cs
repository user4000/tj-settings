﻿using TJSettings;
using TJStandard;
using Telerik.WinControls.UI;
using static TJFramework.TJFrameworkManager;

namespace TJSettingsFormTreeView
{
  public class ManagerFormTreeView : ILocalDatabaseOfSettingsEventNotification
  {
    public const string Empty = "";

    public Converter CvManager { get; } = new Converter();

    public ManagerOfLocalDatabase MnSettings { get; internal set; } = null;

    public ManagerOfLocalDatabase DbSettings { get => MnSettings; }

    public FormTreeViewApplicationSettings Settings { get; internal set; } = null;

    public MaintainerFormTreeView TvFormMaintainer { get; private set; } = null;

    public FormTreeView RfFormTreeView { get; private set; } = null;

    public RadPageViewPage PgTreeView { get; private set; } = null;

    public FormTreeViewMode OneOrManyDatabases { get; internal set; }

    public int ImageIndexCircleGrey { get; } = 21;

    public int ImageIndexCircleYellow { get; } = 22;

    public int ImageIndexCircleRed { get; } = 23;

    public int ImageIndexCircleBlue { get; } = 24;

    public int ImageIndexCircleGreen { get; } = 25;

    public void EventPageChanged(string name)
    {
      if (name.Contains(nameof(TJFramework.Form.FxExit))) return;
      if (TvFormMaintainer.WorkWithOnlyOneDatabase)
        PgTreeView.ZzShow // "Advanced settings" page is visible only if user visits "Settings" page //
          (
          name.Contains(nameof(FormTreeView)) ||
          name.Contains(nameof(TJFramework.Form.FxSettings)) ||
          (!Settings.AutoHideTreeViewSettingsTab)
          );

      TvFormMaintainer.EventPageChanged(name);
    }

    public void EventAfterAllFormsAreCreated()
    {
      RfFormTreeView = Pages.GetRadForm<FormTreeView>();
      TvFormMaintainer = MaintainerFormTreeView.Create(this, OneOrManyDatabases);
      PgTreeView = Pages.GetPageByUniqueName(Pages.GetUniquePageName<FormTreeView>());
      PgTreeView.ZzShow(!Settings.AutoHideTreeViewSettingsTab);
    }

    public void EventBeforeMainFormClose()
    {      
      TvFormMaintainer.EventEndWork();
    }

    public void EventNotification(Notification msg, ReturnCode code, object subscriber, string subscriberName)
    {
      TvFormMaintainer.EventNotification(msg, code, subscriber, subscriberName);
    }
  }
}