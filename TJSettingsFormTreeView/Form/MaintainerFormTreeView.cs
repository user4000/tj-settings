﻿using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using TJFramework;
using TJSettings;
using TJStandard;
using static TJFramework.TJFrameworkManager;

namespace TJSettingsFormTreeView
{
  public class MaintainerFormTreeView : ILocalDatabaseOfSettingsEventNotification
  {
    private int HeightCollapsed { get; } = 126; // default value = 82
    private int HeightExpanded { get; } = 126; // default value = 126
    private int HeightForLongText { get; } = 200; // default value = 200;
    private TreeviewControlManager TvManager { get; set; } = null;
    private GridSettings VxGridSettings { get; set; } = null;
    private DataTable TableFolders { get; set; } = null;
    private string NameOfSelectedNode { get; set; } = string.Empty;
    private RadTreeNode[] SearchResult { get; set; } = null;
    private Setting CurrentSetting { get; set; } = null;
    private string CurrentIdSetting { get; set; } = string.Empty;

    private static readonly long idFolderNoSelection = -1;

    private long CurrentIdFolder { get; set; } = idFolderNoSelection;

    private string CurrentFolderPath { get; set; } = string.Empty;

    private long IdFolderNoValue { get; } = idFolderNoSelection;

    private int SearchIterator { get; set; } = 0;

    public bool WorkWithOnlyOneDatabase { get; private set; } = false;

    public bool FlagAllowChangeSelectedFolder { get; private set; } = true;

    public void AllowChangeSelectedFolder(bool allow) => FlagAllowChangeSelectedFolder = allow;

    public FormTreeView Form { get; private set; } = null;

    public ManagerFormTreeView Manager { get; private set; } = null;

    public ManagerOfLocalDatabase DbSettings { get => Manager.DbSettings; }

    private Dictionary<Tuple<long, string>, string> DcSettings = new Dictionary<Tuple<long, string>, string>();

    private System.Windows.Forms.Timer TmSettings = new System.Windows.Forms.Timer();

    private MaintainerFormTreeView(ManagerFormTreeView manager)
    {
      Manager = manager;
      Form = manager.RfFormTreeView;
      TmSettings.Interval = 2000;
      //TmSettings.Tick += EventTimerSettingUpdate;
    }

    public static MaintainerFormTreeView Create(ManagerFormTreeView manager, FormTreeViewMode mode)
    {
      MaintainerFormTreeView TvFormMaintainer = new MaintainerFormTreeView(manager);
      TvFormMaintainer.Init();
      TvFormMaintainer.WorkWithOnlyOneDatabase = mode == FormTreeViewMode.OneLocalSettingsDatabase;
      TvFormMaintainer.TvManager = TreeviewControlManager.Create(manager);

      if (TvFormMaintainer.WorkWithOnlyOneDatabase)
      {
        TvFormMaintainer.EventLoadData();
        TvFormMaintainer.EventAfterAllFormsAreCreated();
      }

      TvFormMaintainer.UserCanChangeDatabase(!TvFormMaintainer.WorkWithOnlyOneDatabase);

      return TvFormMaintainer;
    }

    internal void EventAfterAllFormsAreCreated()
    {
      // This method places page with "TreeView" form AFTER already created "Settings" page //
      var pageView = TJFrameworkManager.Service.MainPageView;
      var pages = pageView.Pages;
      RadPageViewPage PgSettings = null;
      RadPageViewPage PgTreeView = null;
      RadPageViewPage PgLog = null;
      foreach (var page in pages)
      {
        if (page.Name.Contains(nameof(TJFramework.Form.FxSettings))) PgSettings = page;
        if (page.Name.Contains(nameof(TJFramework.Form.FxLog))) PgLog = page;
        if (page.Name.Contains(nameof(FormTreeView))) PgTreeView = page;
      }
      int index = pageView.Pages.IndexOf(PgLog);
      pageView.Pages.ChangeIndex(PgLog, index - 1);

      index = pageView.Pages.IndexOf(PgSettings);
      pageView.Pages.ChangeIndex(PgTreeView, index);
    }

    private void Init()
    {
      SetProperties();
      SetEvents();
    }

    private void SetProperties()
    {
      Padding NoPadding = new Padding(0, 0, 0, 0);
      Form.BxOpenFile.ShowBorder = false;
      Form.BxSelectFile.ShowBorder = false;
      Form.BxFolderAdd.ShowBorder = false;
      Form.BxFolderRename.ShowBorder = false;
      Form.BxFolderDelete.ShowBorder = false;
      Form.BxFolderSearch.ShowBorder = false;
      Form.BxCopyFolderPath.ShowBorder = false;
      Form.BxGridRefresh.ShowBorder = false;
      Form.BxFolderSearchGotoNext.ShowBorder = false;
      Form.BxFolderSearchGotoNext.Visibility = ElementVisibility.Collapsed;
      Form.BxSettingAddNew.ShowBorder = false;
      Form.BxSettingRename.ShowBorder = false;
      Form.BxSettingFileSelect.ShowBorder = false;
      Form.BxSettingColorSelect.ShowBorder = false;
      Form.BxSettingFontSelect.ShowBorder = false;
      Form.BxSettingFolderSelect.ShowBorder = false;
      Form.BxSettingDelete.ShowBorder = false;
      Form.BxSettingChange.Enabled = false;
      Form.BxSettingSave.Visible = false;
      Form.BxSettingCancel.Visible = false;
      Form.BxNewFileName.ShowBorder = false;
      Form.BxCreateNewDatabase.ShowBorder = false;

      Form.BxCopyFolderPath.ToolTipText = "Copy to clipboard";
      Form.BxCopyFolderPath.AutoToolTip = true;

      Form.BxGridRefresh.ToolTipText = "Refresh list of settings";
      Form.BxGridRefresh.AutoToolTip = true;

      SetPropertiesDateTimePicker();

      Form.TxDatabaseFile.ReadOnly = true;
      Form.TxFolderDelete.ReadOnly = true;

      Form.StxFolder.ReadOnly = !Manager.Settings.AllowEditSettingFolderName;
      Form.StxFile.ReadOnly = !Manager.Settings.AllowEditSettingFileName;

      Form.PvEditor.SelectedPage = Form.PgEmpty;
      Form.PvEditor.ZzPagesVisibility(ElementVisibility.Collapsed);

      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderDelete, 4);
      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderRename, 3);
      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderAdd, 2);
      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderSearch, 1);
      Form.PvFolders.Pages.ChangeIndex(Form.PgDatabase, 0);
      Form.PvFolders.SelectedPage = Form.PgDatabase;

      Form.BxSettingUp.Left = Form.BxSettingCancel.Location.X + Form.BxSettingCancel.Size.Width + 2 * Form.BxSettingUp.Size.Width;
      Form.BxSettingDown.Left = Form.BxSettingCancel.Location.X + Form.BxSettingCancel.Size.Width + 4 * Form.BxSettingUp.Size.Width;

      Form.PvSettings.Pages.ChangeIndex(Form.PgSettingChange, 0);
      Form.PvSettings.SelectedPage = Form.PgSettingChange;

      Form.PnTreeview.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
      Form.PnTreeview.SizeInfo.AbsoluteSize = Manager.Settings.TreeViewSize;

      Form.PnSettingAddTool.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;
      Form.PnSettingChangeTool.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;
      Form.PnSettingAddTop.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;
      Form.PnSettingChangeTop.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;

      Form.StxLongInteger.ZzSetIntegerNumberOnly();
      Form.StxDatetime.CalendarSize = new Size(400, 350);

      VxGridSettings = new GridSettings(this.Manager);
      VxGridSettings.InitializeGrid(Form.GvSettings);

      Form.PgSettingEmpty.Item.Visibility = ElementVisibility.Collapsed;
      Form.PgSettingMessage.Item.Visibility = ElementVisibility.Collapsed;

      Form.PvEditor.Padding = new Padding(0, 1, 0, 0);
      Form.PvEditor.Margin = NoPadding;

      Form.PnSettingAddTool.Padding = NoPadding;
      Form.PnSettingAddTool.Margin = NoPadding;

      Form.PnSettingChangeTool.Padding = NoPadding;
      Form.PnSettingChangeTool.Margin = NoPadding;

      Form.StxDatetime.Value = DateTime.Today;
      Form.TvFolders.ShowRootLines = false;

      SetDatabaseFile(Manager.Settings.SettingsDatabaseLocation);
    }

    private void SetEvents()
    {
      Form.BxOpenFile.Click += async (s, e) => await EventLoadDataAsync(s, e); // Open a database file //
      Form.BxSelectFile.Click += EventButtonChooseFile;
      Form.BxFolderAdd.Click += EventButtonAddFolder;
      Form.BxFolderRename.Click += EventButtonRenameFolder;
      Form.BxFolderDelete.Click += EventButtonDeleteFolder;
      Form.BxFolderSearch.Click += EventButtonSearchFolder;
      Form.BxFolderSearchGotoNext.Click += EventButtonSearchFolderGotoNext;

      Form.BxSettingAddNew.Click += async (s, e) => await EventButtonSettingAddNew(s, e); // SAVE setting (INSERT) //
      Form.BxSettingSave.Click += async (s, e) => await EventButtonSettingUpdateExisting(s, e); // SAVE setting (UPDATE) //
      Form.BxSettingDelete.Click += async (s, e) => await EventButtonSettingDelete(s, e); // Delete setting //
      Form.BxSettingRename.Click += async (s, e) => await EventButtonSettingRename(s, e); // Rename setting //
      Form.BxSettingChange.Click += EventButtonSettingChange; // Change value of setting // it is async method and await works inside it //
      Form.BxSettingCancel.Click += EventButtonSettingCancel; // Cancel changing setting //
      Form.BxSettingUp.Click += async (s, e) => await EventButtonSettingUp(s, e); // Change Rank - go up //
      Form.BxSettingDown.Click += async (s, e) => await EventButtonSettingDown(s, e); // Change Rank - go down //

      Form.BxNewFileName.Click += EventButtonNewFileName;
      Form.BxCreateNewDatabase.Click += EventButtonCreateNewDatabase;

      Form.PvSettings.SelectedPageChanging += EventForAllPageViewSelectedPageChanging;
      Form.PvFolders.SelectedPageChanging += EventForAllPageViewSelectedPageChanging;
      Form.PvSettings.SelectedPageChanging += EventForAllPageViewSelectedPageChanging;

      Form.BxSettingFileSelect.Click += EventButtonSettingFileSelect;
      Form.BxSettingFolderSelect.Click += EventButtonSettingFolderSelect;
      Form.BxSettingColorSelect.Click += EventButtonSettingColorSelect;
      Form.BxSettingFontSelect.Click += EventButtonSettingFontSelect;
      Form.BxGridRefresh.Click += EventRefreshGridSettings;
      Form.BxCopyFolderPath.Click += EventCopyFolderPathToClipboard;

      Form.DxTypes.SelectedValueChanged += EventSettingTypeChanged; // Select TYPE of a NEW variable //

      Form.PvSettings.SelectedPageChanged += EventSettingsSelectedPageChanged; // Settings Change, Add, Rename, Delete //

      Form.TvFolders.SelectedNodeChanged += async (s, e) => await EventTreeviewSelectedNodeChanged(s, e); // SELECTED NODE CHANGED //
      Form.ScMain.SplitterMoved += EventScMainSplitterMoved;
      Form.GvSettings.SelectionChanged += EventGridSelectionChanged; // User has selected a new row //
      Form.GvSettings.CellDoubleClick += EventGridCellDoubleClick;
    }

    private async void EventGridCellDoubleClick(object sender, GridViewCellEventArgs e)
    {
      GridViewDataRowInfo row = (e.Row as GridViewDataRowInfo);
      if (row == null) return;
      Setting setting = row.DataBoundItem as Setting;
      if (setting == null) return;
      await EventUserWantsToChangeSetting(setting);
    }

    private async Task EventUserWantsToChangeSetting(Setting setting)
    {
      if (Form.PvSettings.SelectedPage != Form.PgSettingChange) Form.PvSettings.SelectedPage = Form.PgSettingChange;
      await EventButtonSettingChange();
    }

    private async void EventCopyFolderPathToClipboard(object sender, EventArgs e)
    {
      Form.BxCopyFolderPath.Visibility = ElementVisibility.Collapsed;
      Clipboard.SetText(Form.TxFolderFullPath.Text);
      await Task.Delay(250);
      Form.BxCopyFolderPath.Visibility = ElementVisibility.Visible;
    }

    private async void EventRefreshGridSettings(object sender, EventArgs e)
    {
      Form.BxGridRefresh.Visibility = ElementVisibility.Hidden;
      await RefreshGridSettingsAndClearSelection();
      await Task.Delay(250);
      Form.BxGridRefresh.Visibility = ElementVisibility.Visible;
    }

    private void UserCanChangeDatabase(bool Allow)
    {
      Form.BxSelectFile.Enabled = Allow;
      Form.BxSelectFile.Visibility = Allow ? ElementVisibility.Visible : ElementVisibility.Collapsed;
    }

    private void ResetView()
    {
      ResetDataSourceForTreeview();
      VxGridSettings.ResetView();
      CurrentIdFolder = IdFolderNoValue;
      NameOfSelectedNode = string.Empty;
      CurrentIdSetting = string.Empty;
      CurrentSetting = null;
      SearchResult = null;
      SearchIterator = 0;
      Form.PvSettings.Visible = false;
    }

    private void ResetDataSourceForTreeview() => Form.TvFolders.DataSource = null;

    private void SetDatabaseFile(string PathToDatabaseFile)
    {
      ResetView();
      if (String.IsNullOrEmpty(PathToDatabaseFile)) return;
      Form.TxDatabaseFile.Text = PathToDatabaseFile;
      Form.TxDatabaseFile.SelectionStart = PathToDatabaseFile.Length;
    }

    private void SetPropertiesDateTimePicker()
    {
      Form.StxDatetime.DateTimePickerElement.ShowTimePicker = true;
      Form.StxDatetime.Format = DateTimePickerFormat.Custom;
      Form.StxDatetime.CustomFormat = Manager.CvManager.CvDatetime.DatetimeFormat;
    }

    private void EventForAllPageViewSelectedPageChanging(object sender, RadPageViewCancelEventArgs e)
    {
      e.Cancel = !FlagAllowChangeSelectedFolder;
    }

    private void ShowNotification(bool Success, string Message)
    {
      if (Message.Length < 1)
      {
        Form.PicSettingMessage.Image = null; Form.LxSettingMessage.Text = string.Empty;
      }
      else
      {
        if (Success)
        {
          Form.PicSettingMessage.Image = Form.PicOk.Image; Form.LxSettingMessage.ForeColor = Color.DarkGreen;
        }
        else
        {
          Form.PicSettingMessage.Image = Form.PicError.Image; Form.LxSettingMessage.ForeColor = Color.DarkViolet;
        }
        Form.LxSettingMessage.Text = Message;
      }
      Form.PvSettings.SelectedPage = Form.PgSettingMessage;
      Form.PgSettingMessage.Select(); // <-- If we do not do that the row of the grid remains selected //
    }

    private async Task EventButtonSettingRename(object sender, EventArgs e)
    {
      ReturnCode code = ReturnCodeFactory.Error("An error occurred while trying to change the setting name");
      string NameSettingDraft = Form.TxSettingRename.Text.Trim();
      string NameSetting = NameSettingDraft.RemoveSpecialCharacters();
      Form.TxSettingRename.Text = NameSetting;

      if (NameSetting.Length < 1)
        if (NameSettingDraft.Length < 1)
        {
          Ms.ShortMessage(MsgType.Fail, "Missing new setting name", 350, Form.TxSettingRename).Create();
          return;
        }
        else
        {
          Ms.ShortMessage(MsgType.Fail, "You have specified characters that cannot be used in the name of setting", 500, Form.TxSettingRename).Create();
          return;
        }

      const string ErrorHeader = "Failed to change setting name";
      try
      {
        code = Manager.DbSettings.SettingRename(CurrentIdFolder, CurrentIdSetting, NameSetting);
      }
      catch (Exception ex)
      {
        Ms.Error(ErrorHeader, ex).Control(Form.TxSettingRename).Create();
        return;
      }

      if (code.Success)
      {
        await RefreshGridSettingsAndClearSelection(); // Setting was renamed //
      }
      else
      {
        Ms.Message(ErrorHeader, code.Message).Wire(Form.TxSettingRename).Warning();
      }
      ShowNotification(code.Success, code.Message);
    }

    private async Task EventButtonSettingDelete(object sender, EventArgs e)
    {
      const string ErrorHeader = "Error trying to delete a setting";
      ReturnCode code = ReturnCodeFactory.Error(ErrorHeader);
      string NameSetting = CurrentIdSetting;
      try
      {
        code = DbSettings.SettingDelete(CurrentIdFolder, CurrentIdSetting);
      }
      catch (Exception ex)
      {
        Ms.Error(ErrorHeader, ex).Control(Form.TxSettingRename).Create();
        return;
      }

      if (code.Success)
      {
        await RefreshGridSettingsAndClearSelection(); // Setting was deleted //
      }
      else
      {
        Ms.Message(ErrorHeader, code.Message).Wire(Form.TxSettingDelete).Warning();
      }
      ShowNotification(code.Success, code.Message);
    }

    private void EventButtonSettingFontSelect(object sender, EventArgs e)
    {
      FontDialog dialog = new FontDialog();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxFont.Text = Manager.CvManager.CvFont.ToString(dialog.Font);
    }

    private void EventButtonSettingColorSelect(object sender, EventArgs e)
    {
      RadColorDialog dialog = new RadColorDialog();
      if ((Form.StxColor.Text.Length > 0) && (Form.PvSettings.SelectedPage == Form.PgSettingChange)) dialog.SelectedColor = Manager.CvManager.CvColor.FromString(Form.StxColor.Text).Value;
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxColor.Text = Manager.CvManager.CvColor.ToString(dialog.SelectedColor);
    }

    private void EventButtonSettingFolderSelect(object sender, EventArgs e)
    {
      RadOpenFolderDialog dialog = new RadOpenFolderDialog();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxFolder.Text = dialog.FileName;
    }

    private void EventButtonSettingFileSelect(object sender, EventArgs e)
    {
      RadOpenFileDialog dialog = new RadOpenFileDialog();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxFile.Text = dialog.FileName;
    }

    private void EventSettingsSelectedPageChanged(object sender, EventArgs e)
    {
      if ((Form.PvSettings.SelectedPage == Form.PgSettingAdd) && (Form.PvEditor.Parent != Form.PnSettingAddTool))
      {
        PanelSettingsChangeSizeBySettingType(TypeSetting.Unknown);
        Form.PvEditor.Parent = Form.PnSettingAddTool;
      }
      Form.DxTypes.SelectedIndex = (int)(TypeSetting.Unknown); // Unknown //
      SettingEditorResetAllInputControls();
    }

    private void SettingEditorResetAllInputControls()
    {
      Form.StxBoolean.Value = false;
      Form.StxDatetime.Value = DateTime.Today;
      Form.StxFile.Clear();
      Form.StxFolder.Clear();
      Form.StxLongInteger.Clear();
      Form.StxPassword.Clear();
      Form.StxText.Clear();
      Form.StxFont.Clear();
      Form.StxColor.Clear();
    }

    private int GetMessageBoxWidth(string message) => Math.Min(message.Length * 9, 500);

    internal TypeSetting GetTypeFromDropDownList()
    {
      int integerValue = 0;
      try { integerValue = Form.DxTypes.ZzGetIntegerValue(); } catch { integerValue = -1; };
      if (integerValue < 0) return TypeSetting.Unknown;
      return TypeSettingConverter.FromInteger(integerValue);
    }

    private void EventScMainSplitterMoved(object sender, SplitterEventArgs e)
    {
      if (Form.PnTreeview.SizeInfo.AbsoluteSize.Width > (2 * Form.PnUpper.Width) / 3)
        Form.PnTreeview.SizeInfo.AbsoluteSize = new Size((39 * Form.PnUpper.Width) / 100, 0);
    }

    private void EventSettingOneRowSelected() // Event - user selected a setting in the grid //
    {
      bool OneRowSelected = CurrentIdSetting.Length > 0;

      Form.BxSettingChange.Enabled = OneRowSelected;
      Form.BxSettingUp.Enabled = OneRowSelected;
      Form.BxSettingDown.Enabled = OneRowSelected;

      Form.TxSettingRename.Text = CurrentIdSetting;
      Form.TxSettingDelete.Text = CurrentIdSetting;

      if (Form.PgSettingDelete.Enabled != OneRowSelected) Form.PgSettingDelete.Enabled = OneRowSelected;
      if (Form.PgSettingRename.Enabled != OneRowSelected) Form.PgSettingRename.Enabled = OneRowSelected;
      if (Form.PgSettingChange.Enabled != OneRowSelected) Form.PgSettingChange.Enabled = OneRowSelected;

      if (OneRowSelected && (Form.PvSettings.SelectedPage == Form.PgSettingMessage))
      {
        Form.PvSettings.SelectedPage = Form.PgSettingEmpty;
      }
    }

    private async Task RefreshGridSettings()
    {
      var list = await DbSettings.GetSettingsAsync(CurrentIdFolder);
      VxGridSettings.RefreshGrid(list);
    }

    private void EventSettingClearSelection()
    {
      CurrentIdSetting = string.Empty;
      CurrentSetting = null;

      Form.PgSettingDelete.Enabled = false;
      Form.PgSettingRename.Enabled = false;
      Form.PgSettingChange.Enabled = false;
      if (Form.PvSettings.SelectedPage != Form.PgSettingEmpty) Form.PvSettings.SelectedPage = Form.PgSettingEmpty;
    }

    private async Task RefreshGridSettingsAndClearSelection()
    {
      await RefreshGridSettings();
      if (Form.PvSettings.Visible == false) Form.PvSettings.Visible = true;
      EventSettingClearSelection();
      VxGridSettings.Grid.HideSelection = true;
      VxGridSettings.Grid.GridNavigator.ClearSelection();
      SetMarkGridNeedsToBeRefreshed(false);
    }

    private async Task EventTreeviewSelectedNodeChanged(object sender, RadTreeViewEventArgs e)
    {
      Form.TvFolders.HideSelection = true;

      try
      {
        NameOfSelectedNode = e.Node.Text;
      }
      catch
      {
        NameOfSelectedNode = string.Empty;
      }
      Form.TxFolderRename.Text = NameOfSelectedNode;
      Form.TxFolderDelete.Text = NameOfSelectedNode;

      CurrentIdFolder = DbSettings.GetIdFolder(e.Node);
      CurrentFolderPath = CurrentIdFolder >= 0 ? e.Node.FullPath : string.Empty;
      Form.TxFolderFullPath.Text = CurrentFolderPath;

      await RefreshGridSettingsAndClearSelection(); // current treeview node was changed //

      if ((Form.PvSettings.SelectedPage == Form.PgSettingRename) || (Form.PvSettings.SelectedPage == Form.PgSettingDelete)) Form.PvSettings.SelectedPage = Form.PgSettingEmpty;

      Form.TvFolders.HideSelection = false;
    }

    private void EventGridSelectionChanged(object sender, EventArgs e)
    {
      CurrentIdSetting = VxGridSettings.GetIdSetting();
      if (VxGridSettings.Grid.HideSelection) VxGridSettings.Grid.HideSelection = false;
      CurrentSetting = VxGridSettings.GetSetting(CurrentIdSetting);
      EventSettingOneRowSelected();
    }

    private void PutValueOfCurrentSettingToInputControl()
    {
      switch ((TypeSetting)CurrentSetting.IdType)
      {
        case TypeSetting.Boolean:
          Form.StxBoolean.Value = Manager.CvManager.CvBoolean.FromString(CurrentSetting.SettingValue).Value;
          break;
        case TypeSetting.Datetime:
          Form.StxDatetime.Value = Manager.CvManager.CvDatetime.FromString(CurrentSetting.SettingValue).Value;
          break;
        case TypeSetting.Integer64:
          Form.StxLongInteger.Text = Manager.CvManager.CvInt64.FromString(CurrentSetting.SettingValue).Value.ToString();
          break;
        case TypeSetting.Text:
          Form.StxText.Text = CurrentSetting.SettingValue;
          break;
        case TypeSetting.Password:
          Form.StxPassword.Text = string.Empty;
          break;
        case TypeSetting.File:
          Form.StxFile.Text = CurrentSetting.SettingValue;
          break;
        case TypeSetting.Folder:
          Form.StxFolder.Text = CurrentSetting.SettingValue;
          break;
        case TypeSetting.Font:
          Form.StxFont.Text = CurrentSetting.SettingValue;
          break;
        case TypeSetting.Color:
          Form.StxColor.Text = CurrentSetting.SettingValue;
          break;
        default:
          break;
      }
    }

    private async void EventButtonSettingChange(object sender, EventArgs e)
    {
      await EventButtonSettingChange();
    }

    private async Task EventButtonSettingChange()
    {
      PutValueOfCurrentSettingToInputControl();

      Form.BxSettingDown.Enabled = false;
      Form.BxSettingUp.Enabled = false;
      Form.BxGridRefresh.Enabled = false;
      Form.BxSettingChange.Enabled = false;
      Form.BxSettingSave.Visible = true;
      Form.BxSettingCancel.Visible = true;

      AllowChangeSelectedFolder(false);

      Form.TxDatabaseFile.Enabled = false;
      Form.TxFolderDelete.Enabled = false;
      Form.TxFolderSearch.Enabled = false;
      Form.TxFolderRename.Enabled = false;
      Form.TxFolderAdd.Enabled = false;

      if (Form.PvEditor.Parent != Form.PnSettingChangeTool) Form.PvEditor.Parent = Form.PnSettingChangeTool;
      await Task.Delay(100);
      PanelSettingsChangeSizeBySettingType((TypeSetting)CurrentSetting.IdType);
    }

    private void EventButtonSettingCancel(object sender, EventArgs e)
    {
      EventButtonSettingCancel();
      Form.BxSettingDown.Enabled = true;
      Form.BxSettingUp.Enabled = true;
    }

    private void EventButtonSettingCancel()
    {
      Form.BxSettingChange.Enabled = true;
      Form.BxGridRefresh.Enabled = true;
      Form.BxSettingSave.Visible = false;
      Form.BxSettingCancel.Visible = false;

      AllowChangeSelectedFolder(true);

      Form.TxDatabaseFile.Enabled = true;
      Form.TxFolderDelete.Enabled = true;
      Form.TxFolderSearch.Enabled = true;
      Form.TxFolderRename.Enabled = true;
      Form.TxFolderAdd.Enabled = true;

      PanelSettingsChangeSizeBySettingType(TypeSetting.Unknown);
    }

    private void SelectOneNode(RadTreeNode node)
    {
      if (node != null)
        try
        {
          node.Selected = true;
          node.EnsureVisible();
        }
        catch
        {
          ClearArraySearchResult();
        }
    }

    private void EventButtonAddFolder(object sender, EventArgs e)
    {
      const string ErrorHeader = "Error trying to add a new folder";
      RadTreeNode parent = Form.TvFolders.SelectedNode;
      ReturnCode code = ReturnCodeFactory.Error(ErrorHeader);

      if (parent == null)
      {
        Ms.ShortMessage(MsgType.Fail, "The folder where the new one is added is not specified", 400, Form.TxFolderAdd).Create();
        return;
      }

      long IdFolder = DbSettings.GetIdFolder(parent);
      string ParentFullPath = parent.FullPath;
      bool Error = false;

      if (IdFolder < 0)
      {
        Ms.ShortMessage(MsgType.Fail, "The folder where the new one is added is not specified", 400, Form.TxFolderAdd).Create();
        return;
      }

      if (parent.Level > 14)
      {
        Ms.ShortMessage(MsgType.Fail, "Hierarchical nesting too deep", 280, Form.TxFolderAdd).Create();
        return;
      }

      string NameFolderDraft = Form.TxFolderAdd.Text.Trim();
      string NameFolder = NameFolderDraft.RemoveSpecialCharacters();
      Form.TxFolderAdd.Text = NameFolder;

      if (NameFolder.Length < 1)
        if (NameFolderDraft.Length < 1)
        {
          Ms.ShortMessage(MsgType.Fail, "New folder name not specified", 230, Form.TxFolderAdd).Offset(-100 + Form.TxFolderAdd.Size.Width, -70).Create();
          return;
        }
        else
        {
          Ms.ShortMessage(MsgType.Fail, "You have specified characters that cannot be used in the folder name", 470, Form.TxFolderAdd).Create();
          return;
        }

      long IdNewFolder = IdFolderNoValue;

      try
      {
        code = DbSettings.FolderInsert(IdFolder, NameFolder);
        IdNewFolder = code.IdObject;
      }
      catch (Exception ex)
      {
        Error = true;
        Ms.Error(ErrorHeader, ex).Control(Form.TxFolderAdd).Create();
      }

      if (code.Error)
      {
        if (Error == false) Ms.Message(ErrorHeader, code.Message, Form.TxFolderAdd).Fail();
      }

      if (IdNewFolder <= 0)
      {
        if ((Error == false) && (code.Success)) Ms.Message(ErrorHeader, code.Message, Form.TxFolderAdd).Fail();
      }
      else // Give out SUCCESS MESSAGE //
      {
        if (NameFolderDraft.Length == NameFolder.Length)
        {
          Ms.ShortMessage(MsgType.Debug, code.Message, GetMessageBoxWidth(code.Message), Form.TxFolderAdd)
            .Offset(Form.TxFolderAdd.Width, -5 * Form.TxFolderAdd.Height).Create();
        }
        else
        {
          Ms.Message("Some characters you specify\nhave been excluded from the name", code.Message)
            .Control(Form.TxFolderAdd).Offset(200, 0).Delay(7).Info();
          Ms.Message("The folder name contained forbidden characters:", NameFolderDraft).NoAlert().Warning();
          Ms.Message("The name has been corrected:", NameFolder).NoAlert().Warning();
        }

        EventRefreshDataFromDatabaseFile();

        parent = Form.TvFolders.GetNodeByPath(ParentFullPath);
        if (parent == null)
        {
          Ms.Message(MsgType.Error, "Error!", $"Method TvFolders.GetNodeByPath(ParentFullPath) has returned [null]. ParentFullPath={ParentFullPath}", null, MsgPos.Unknown, 0).NoAlert().Create();
          Ms.ShortMessage(MsgType.Warning, $"Error! Details in the message log", 300, Form.TxFolderAdd).NoTable().Create();
        }
        else
        {
          TvManager.TryToSelectFolderAfterCreating(parent, NameFolder);
        }
      }
      Form.TxFolderAdd.Clear();
    }

    private void EventButtonRenameFolder(object sender, EventArgs e)
    {
      const string ErrorHeader = "Failed to rename folder";
      RadTreeNode node = Form.TvFolders.SelectedNode;
      ReturnCode code = ReturnCodeFactory.Error(ErrorHeader);

      if (node == null)
      {
        Ms.ShortMessage(MsgType.Fail, "The folder you want to rename is not specified", 380, Form.TxFolderRename).Create();
        return;
      }

      long IdFolder = DbSettings.GetIdFolder(node);
      string NodeFullPath = node.FullPath;

      if (IdFolder < 0)
      {
        Ms.ShortMessage(MsgType.Fail, "The folder you want to rename is not specified", 350, Form.TxFolderRename).Create();
        return;
      }

      string NameFolder = Form.TxFolderRename.Text.RemoveSpecialCharacters();
      Form.TxFolderRename.Text = NameFolder;

      if (NameFolder.Length < 1)
      {
        Ms.ShortMessage(MsgType.Fail, "No new folder name specified", 340, Form.TxFolderRename).Create();
        return;
      }

      if (node.Text == NameFolder)
      {
        Ms.ShortMessage(MsgType.Fail, "The new name is no different from the previous one", 400, Form.TxFolderRename).Create();
        return;
      }

      try
      {
        code = DbSettings.FolderRename(IdFolder, NameFolder);
      }
      catch (Exception ex)
      {
        Ms.Error(ErrorHeader, ex).Control(Form.TxFolderRename).Create();
        return;
      }

      if (code.Success)
      {
        Ms.ShortMessage(MsgType.Debug, code.Message, GetMessageBoxWidth(code.Message), Form.TxFolderRename).Create();
        ClearArraySearchResult();
        node.Text = NameFolder;
        node.Selected = false;
        node.Selected = true; //-V3008
      }
      else
      {
        Ms.Message(ErrorHeader, code.Message, Form.TxFolderRename).Fail();
      }
    }

    private void EventButtonDeleteFolder(object sender, EventArgs e)
    {
      const string ErrorHeader = "Failed to delete folder";
      RadTreeNode node = Form.TvFolders.SelectedNode;
      ReturnCode code = ReturnCodeFactory.Error(ErrorHeader);

      if (node == null)
      {
        Ms.ShortMessage(MsgType.Fail, "The folder you want to delete is not specified", 350, Form.TxFolderDelete).Create();
        return;
      }

      if (node.Level == 0)
      {
        Ms.ShortMessage(MsgType.Warning, "It is not allowed to delete a root folder", 350, Form.TxFolderDelete).Create();
        return;
      }

      RadTreeNode parent = node.Parent;
      if (parent == null)
      {
        Ms.ShortMessage(MsgType.Fail, "The folder containing the folder to be deleted was not found", 450, Form.TxFolderDelete).Create();
        return;
      }

      string NodeFullPath = parent.FullPath;
      long IdFolder = DbSettings.GetIdFolder(node);

      try
      {
        code = DbSettings.FolderDelete(IdFolder, node.Text);
      }
      catch (Exception ex)
      {
        Ms.Error(ErrorHeader, ex).Control(Form.TxFolderDelete).Create();
        return;
      }

      if (code.Success)
      {
        Ms.ShortMessage(MsgType.Debug, code.Message, GetMessageBoxWidth(code.Message), Form.TxFolderDelete).Create();
        parent.Selected = true;
        ClearArraySearchResult();
        node.Remove();
        if (parent.Nodes.Count < 1) parent.ImageIndex = 0;
      }
      else
      {
        Ms.Message(ErrorHeader, code.Message, Form.TxFolderDelete).Fail();
      }
    }

    private void ClearArraySearchResult()
    {
      if ((SearchResult != null) && (SearchResult.Length > 0))
      {
        Array.Clear(SearchResult, 0, SearchResult.Length);
        SearchIterator = 0;
      }
      if (Form.BxFolderSearchGotoNext.Visibility == ElementVisibility.Visible) Form.BxFolderSearchGotoNext.Visibility = ElementVisibility.Collapsed;
    }

    private void EventButtonSearchFolder(object sender, EventArgs e)
    {
      string NameFolder = Form.TxFolderSearch.Text;
      if (SearchResult?.Length > 0) ClearArraySearchResult();

      Point offset = new Point(Form.PvFolders.Width, -1 * Form.PvFolders.Height);

      if (NameFolder.Length < 1)
      {
        Ms.ShortMessage(MsgType.Fail, "No characters to search", 250, Form.PvFolders).Offset(offset).NoTable().Create(); return;
      }

      if (Manager.Settings.FolderNameSearchMode == TextSearchMode.StartWith)
        SearchResult = Form.TvFolders.FindNodes(x => x.Name.StartsWith(NameFolder));

      if (Manager.Settings.FolderNameSearchMode == TextSearchMode.Contains)
        SearchResult = Form.TvFolders.FindNodes(x => x.Name.Contains(NameFolder));

      if (Manager.Settings.FolderNameSearchMode == TextSearchMode.WholeWord)
        SearchResult = Form.TvFolders.FindNodes(x => x.Name == NameFolder);

      SearchIterator = 0;
      if (SearchResult.Length > 0) SelectOneNode(SearchResult[0]);

      Form.BxFolderSearchGotoNext.Visibility = ElementVisibility.Collapsed;

      if (SearchResult.Length < 1)
      {
        Ms.ShortMessage(MsgType.Fail, "The search has not given any results", 300, Form.PvFolders).Offset(offset).NoTable().Create();
      }

      if (SearchResult.Length == 1)
      {
        Ms.ShortMessage(MsgType.Debug, "One folder has been found", 220, Form.PvFolders).Offset(offset).NoTable().Create();
      }

      if (SearchResult.Length > 1)
      {
        Ms.ShortMessage(MsgType.Info, $"Number of folders found: {SearchResult.Length}", 220, Form.PvFolders).Offset(offset).NoTable().Create();
        SearchIterator = 1;
        Form.BxFolderSearchGotoNext.Visibility = ElementVisibility.Visible;
      }
    }

    private void EventButtonSearchFolderGotoNext(object sender, EventArgs e)
    {
      if (SearchResult.Length < 1) return;
      if (SearchResult.Length > SearchIterator)
      {
        SelectOneNode(SearchResult[SearchIterator]);
        SearchIterator++;
        if (SearchResult.Length == SearchIterator)
        {
          Ms.ShortMessage(MsgType.Ok, "Search completed", 200, Form.TxFolderSearch).Offset(new Point(Form.TxFolderSearch.Width, 0)).NoTable().Create();
        }
      }
      else
      {
        SearchIterator = 0;
        SelectOneNode(SearchResult[SearchIterator]);
        SearchIterator++;
      }
    }

    private void EventButtonChooseFile(object sender, EventArgs e)
    {
      Form.DialogOpenFile.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath);
      if (Form.DialogOpenFile.ShowDialog() == DialogResult.OK)
      {
        SetDatabaseFile(Form.DialogOpenFile.FileName);
        Form.DialogOpenFile.InitialDirectory = Path.GetDirectoryName(Form.DialogOpenFile.FileName);
      }
    }

    private void EventLoadData()
    {
      /*
       This method is used only if the form operates in the maintenance mode of 
       only one database without the possibility of changing the location of the database
      */
      if (WorkWithOnlyOneDatabase == false)
      {
        MessageBox.Show("Error [EventLoadData()] method", "This method can be used if [WorkWithOnlyOneDatabase] mode enabled.", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      //Form.BxOpenFile.Enabled = false; Form.BxOpenFile.Visibility = ElementVisibility.Collapsed; // Hide [Load (or reload) data] button //
      Form.PgCreateDatabase.Enabled = false;
      Form.PvFolders.Pages.Remove(Form.PgCreateDatabase);
      EventLoadDataFromDatabaseFile(true);
    }

    private async Task EventLoadDataAsync(object sender, EventArgs e)
    {
      Form.BxOpenFile.Enabled = false; Form.BxOpenFile.Visibility = ElementVisibility.Collapsed;
      EventLoadDataFromDatabaseFile(true);
      await Task.Delay(2000);
      Form.BxOpenFile.Enabled = true; Form.BxOpenFile.Visibility = ElementVisibility.Visible;
    }

    private void EventRefreshDataFromDatabaseFile() => EventLoadDataFromDatabaseFile(false);

    private void EventLoadDataFromDatabaseFile(bool LoadDataFirstTimeFromThisFile)
    {
      DataTable table = null; bool Error = false;
      ReturnCode ConnectToDatabase = ReturnCodeFactory.Success();
      string DatabaseFile = Form.TxDatabaseFile.Text;

      if (String.IsNullOrEmpty(DatabaseFile) || (!File.Exists(DatabaseFile)))
      {
        Ms.Message("Could not get access to the file", "File does not exist").Control(Form.TxDatabaseFile).Error(); return;
      }

      try
      {
        ConnectToDatabase = DbSettings.ConnectToDatabase(DatabaseFile, string.Empty);
        if (ConnectToDatabase.Error) throw new DatabaseStructureCheckException(ConnectToDatabase.Message);
        table = DbSettings.GetTableFolders();
      }
      catch (DatabaseStructureCheckException ex)
      {
        Error = true;
        Ms.Message("Checking database structure", ex.Message).Control(Form.TxDatabaseFile).Error();
      }
      catch (Exception ex)
      {
        Error = true;
        if (ex.Message.Contains("not a database"))
        {
          Ms.Message("Failed to read data from the file", "This file is not a database of the specified type").Control(Form.TxDatabaseFile).Error();
        }
        else
        {
          Ms.Error("Failed to read data from the file you specified", ex).Control(Form.TxDatabaseFile).Create();
        }
      }

      if (Error == false)
        try
        {
          if (TableFolders != null)
          {
            TableFolders.Clear(); //TableFolders.Columns.Clear();// TableFolders.Dispose();
          }
          DbSettings.FillTreeView(Form.TvFolders, table);
          TvManager.SetFontAndImageForAllNodes();
          TableFolders = table;
        }
        catch (Exception ex)
        {
          Error = true;
          Ms.Error("An error occurred while trying to read the settings structure from the file", ex).Control(Form.TxDatabaseFile).Create();
        }

      if (Error == false)
      {
        Manager.Settings.SettingsDatabaseLocation = Form.TxDatabaseFile.Text;
        if (LoadDataFirstTimeFromThisFile) EventLoadDataFromFileFirstTime(DatabaseFile);
      }
    }

    private void EventLoadDataFromFileFirstTime(string DatabaseFile)
    {
      DbSettings.FillDropDownListForTableTypes(Form.DxTypes);
      if (Form.DxTypes.Items.Count < 1)
      {
        MessageBox.Show("Error", "Could not read list of types from a database.", MessageBoxButtons.OK, MessageBoxIcon.Error);
        Ms.Message(MsgType.Error, "", "Could not read list of types from a database.").ToFile().NoAlert().Error();
        return;
      }
      Ms.Message("Data has been loaded from settings database", DatabaseFile).NoAlert().Info();
      /*
      Ms.ShortMessage(MsgType.Info, "   Connected", 120)
        .NoTable().Wire(Form.TxDatabaseFile)
        .Offset(Form.TxDatabaseFile.Width + 10, -50).Delay(1).Create();
      */
    }

    private async Task EventButtonSettingAddNew(object sender, EventArgs e)
    {
      await EventSettingSave(true);
    }

    private async Task EventButtonSettingUpdateExisting(object sender, EventArgs e)
    {
      await EventSettingSave(false);
      EventButtonSettingCancel();
      if (VxGridSettings.Grid.SelectedRows.Count < 1) Form.BxSettingChange.Enabled = false;
    }

    private async Task EventSettingSave(bool AddNewSetting)
    {
      string ErrorHeader = AddNewSetting ? "Failed to create a new setting" : "Failed to change setting value";
      ReturnCode code = ReturnCodeFactory.Error(ErrorHeader);
      string IdSetting = AddNewSetting ? Form.TxSettingAdd.Text.RemoveSpecialCharacters() : CurrentIdSetting;
      Form.TxSettingAdd.Text = AddNewSetting ? IdSetting : string.Empty;

      if (IdSetting.Length < 1)
      {
        Ms.Message("Error", "Setting name not specified").Control(Form.DxTypes).Warning(); return;
      }

      TypeSetting type = AddNewSetting ? GetTypeFromDropDownList() : (TypeSetting)CurrentSetting.IdType;

      switch (type)
      {
        case TypeSetting.Unknown:
          Form.PvEditor.SelectedPage = Form.PgEmpty;
          code = ReturnCodeFactory.Error("Setting type not specified");
          break;
        case TypeSetting.Boolean:
          Form.PvEditor.SelectedPage = Form.PgBoolean;
          code = DbSettings.SaveSettingBoolean(AddNewSetting, CurrentIdFolder, IdSetting, Form.StxBoolean.Value);
          break;
        case TypeSetting.Datetime:
          Form.PvEditor.SelectedPage = Form.PgDatetime;
          code = DbSettings.SaveSettingDatetime(AddNewSetting, CurrentIdFolder, IdSetting, Form.StxDatetime.Value);
          break;
        case TypeSetting.Integer64:
          Form.PvEditor.SelectedPage = Form.PgInteger;
          Form.StxLongInteger.Text = Form.StxLongInteger.Text.Trim();
          if (Manager.CvManager.CvInt64.IsValid(Form.StxLongInteger.Text) == false)
          {
            Ms.Message("Error", "Value is not an integer").Control(Form.PnUpper).Offset(Form.TvFolders.Width, -100).Warning(); return;
          }
          code = DbSettings.SaveSettingInteger64(AddNewSetting, CurrentIdFolder, IdSetting, Manager.CvManager.CvInt64.FromString(Form.StxLongInteger.Text).Value);
          break;
        case TypeSetting.Text:
          Form.PvEditor.SelectedPage = Form.PgText;
          code = DbSettings.SaveSettingText(AddNewSetting, CurrentIdFolder, IdSetting, TypeSetting.Text, Form.StxText.Text);
          break;
        case TypeSetting.Password:
          Form.PvEditor.SelectedPage = Form.PgPassword;
          code = DbSettings.SaveSettingText(AddNewSetting, CurrentIdFolder, IdSetting, TypeSetting.Password, Form.StxPassword.Text);
          break;
        case TypeSetting.File:
          Form.PvEditor.SelectedPage = Form.PgFile;
          code = DbSettings.SaveSettingText(AddNewSetting, CurrentIdFolder, IdSetting, TypeSetting.File, Form.StxFile.Text);
          break;
        case TypeSetting.Folder:
          Form.PvEditor.SelectedPage = Form.PgFolder;
          code = DbSettings.SaveSettingText(AddNewSetting, CurrentIdFolder, IdSetting, TypeSetting.Folder, Form.StxFolder.Text);
          break;
        case TypeSetting.Font:
          Form.PvEditor.SelectedPage = Form.PgFont;
          if (Form.StxFont.Text.Length < 1)
          {
            Ms.Message("Error", "You did not select a font").Control(Form.DxTypes).Warning(); return;
          }
          code = DbSettings.SaveSettingText(AddNewSetting, CurrentIdFolder, IdSetting, TypeSetting.Font, Form.StxFont.Text);
          break;
        case TypeSetting.Color:
          Form.PvEditor.SelectedPage = Form.PgColor;
          if (Form.StxColor.Text.Length < 1)
          {
            Ms.Message("Error", "You did not select a color").Control(Form.DxTypes).Warning(); return;
          }
          code = DbSettings.SaveSettingText(AddNewSetting, CurrentIdFolder, IdSetting, TypeSetting.Color, Form.StxColor.Text);
          break;
        default:
          Form.PvEditor.SelectedPage = Form.PgEmpty;
          break;
      }

      AllowChangeSelectedFolder(true);

      if (code.Success)
      {
        Form.TxSettingAdd.Clear();
        await RefreshGridSettingsAndClearSelection(); // Setting: INSERT or UPDATE //
      }
      else
      {
        Ms.Message("Error", code.Message).Control(Form.DxTypes).Offset(30, -100).NoAlert().Warning();
      }
      ShowNotification(code.Success, code.Message);
    }

    private void PanelSettingsChangeSizeBySettingType(TypeSetting type)
    {
      int height = HeightExpanded;
      switch (type)
      {
        case TypeSetting.Boolean:
          Form.PvEditor.SelectedPage = Form.PgBoolean;
          try { Form.StxBoolean.Focus(); } catch { }
          break;
        case TypeSetting.Datetime:
          Form.PvEditor.SelectedPage = Form.PgDatetime;
          //try { Form.StxDatetime.Focus(); } catch { }
          break;
        case TypeSetting.Integer64:
          Form.PvEditor.SelectedPage = Form.PgInteger;
          try { Form.StxLongInteger.Focus(); } catch { }
          break;
        case TypeSetting.Text:
          Form.PvEditor.SelectedPage = Form.PgText;
          height = HeightForLongText;
          //try { Form.StxText.Focus(); } catch { }
          break;
        case TypeSetting.Password:
          Form.PvEditor.SelectedPage = Form.PgPassword;
          try { Form.StxPassword.Focus(); } catch { }
          break;
        case TypeSetting.File:
          Form.PvEditor.SelectedPage = Form.PgFile;
          try { Form.StxFile.Focus(); } catch { }
          break;
        case TypeSetting.Folder:
          Form.PvEditor.SelectedPage = Form.PgFolder;
          try { Form.StxFolder.Focus(); } catch { }
          break;
        case TypeSetting.Font:
          Form.PvEditor.SelectedPage = Form.PgFont;
          //try { Form.StxFont.Focus(); } catch { }
          break;
        case TypeSetting.Color:
          Form.PvEditor.SelectedPage = Form.PgColor;
          //try { Form.StxColor.Focus(); } catch { }
          break;
        default:
          Form.PvEditor.SelectedPage = Form.PgEmpty;
          height = HeightCollapsed;
          break;
      }
      Form.PvSettings.Height = height;
    }

    private void EventSettingTypeChanged(object sender, EventArgs e)
    {
      PanelSettingsChangeSizeBySettingType(GetTypeFromDropDownList());
    }

    private async Task SettingChangeRank(bool GotoUp)
    {
      if (GotoUp) { Form.BxSettingUp.Enabled = false; } else { Form.BxSettingDown.Enabled = false; }
      string IdSetting = CurrentIdSetting;
      Setting sibling = GotoUp ? VxGridSettings.UpperSibling(CurrentSetting) : VxGridSettings.LowerSibling(CurrentSetting);
      if (sibling != null)
      {
        ReturnCode code = DbSettings.SwapRank(CurrentIdFolder, CurrentSetting, sibling);
        await this.RefreshGridSettingsAndClearSelection(); // Setting: rank changed //
        Form.PnUpper.Select();
      }
    }

    private async Task EventButtonSettingDown(object sender, EventArgs e) => await SettingChangeRank(false);

    private async Task EventButtonSettingUp(object sender, EventArgs e) => await SettingChangeRank(true);

    private void EventButtonNewFileName(object sender, EventArgs e)
    {
      RadSaveFileDialog DxNewFile = new RadSaveFileDialog();
      DxNewFile.InitialDirectory = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
      DxNewFile.FileName = Manager.Settings.NewFileName;
      DialogResult result = DxNewFile.ShowDialog();
      if (result == DialogResult.OK)
      {
        Form.TxCreateDatabase.Text = DxNewFile.FileName;
      }
    }

    private void EventButtonCreateNewDatabase(object sender, EventArgs e)
    {
      ReturnCode code = DbSettings.CreateNewDatabase(Form.TxCreateDatabase.Text);
      if (code.Success)
      {
        Form.PvFolders.SelectedPage = Form.PgDatabase;
        SetDatabaseFile(Form.TxCreateDatabase.Text);
        Form.TxCreateDatabase.Clear();
        Ms.Message("New database created", "Click the button on the right to open a new database").Control(Form.TxDatabaseFile).Ok();
      }
      else
      {
        Ms.Message("Failed to create a new database", code.Message + " " + code.Note).Control(Form.TxDatabaseFile).Error();
      }
    }

    internal void EventPageChanged(string name)
    {
      if (name.Contains(nameof(FormTreeView)))
        EventUserVisitsFormTreeViewPage();
      else
        EventUserLeavesFormTreeViewPage();
    }

    private async void EventUserVisitsFormTreeViewPage() // If user returns to the page then grid is being refreshed //
    {
      if (CurrentIdFolder != IdFolderNoValue) await RefreshGridSettings();
    }

    private void EventUserLeavesFormTreeViewPage()
    {
      if (!FlagAllowChangeSelectedFolder) EventButtonSettingCancel(this, new EventArgs());
    }

    public void EventEndWork()
    {
      Manager.Settings.TreeViewSize = Form.PnTreeview.SizeInfo.AbsoluteSize;
    }

    public string TrimFolderPathSeparator(string FolderPath)
    {
      return FolderPath.TrimStart(DbSettings.FolderPathSeparator[0]).TrimEnd(DbSettings.FolderPathSeparator[0]);
    }

    public bool IsEqual(string FolderPath1, string FolderPath2)
    {
      return TrimFolderPathSeparator(FolderPath1) == TrimFolderPathSeparator(FolderPath2);
    }

    public void EventNotification(Notification msg, ReturnCode code, object subscriber, string subscriberName)
    {
      /*Ms.Message
        (
        $"CurrentIdFolder={CurrentIdFolder}; msg.IdFolder={msg.IdFolder}",
        $"TypeEvent={msg.TypeOfEvent}; intiator={msg.Initiator}; subscriber={subscriber}; FolderPath={msg.FolderPath};"
        ).NoAlert().Debug();*/

      /*
       if (msg.Initiator != subscriber)
         if (CurrentIdFolder >= 0)
           if ((CurrentIdFolder == msg.IdFolder) || (CurrentFolderPath == DbSettings.AddRootFolderNameIfNotSpecified(msg.FolderPath)))
             if ((msg.TypeOfEvent == TypeEvent.SettingCreateOrUpdate) || (msg.TypeOfEvent == TypeEvent.SettingUpdate))
               UpdateSettingValueUsingTimer(msg); // UpdateSettingValue(msg);
       */

      if ((msg.Initiator != subscriber) && (CurrentIdFolder >= 0))
        if ((CurrentIdFolder == msg.IdFolder) || (CurrentFolderPath == DbSettings.AddRootFolderNameIfNotSpecified(msg.FolderPath)))
          if
            (
            (msg.TypeOfEvent == TypeEvent.SettingCreateOrUpdate) ||
            (msg.TypeOfEvent == TypeEvent.SettingUpdate) ||
            (msg.TypeOfEvent == TypeEvent.SettingCreate) ||
            (msg.TypeOfEvent == TypeEvent.SettingDelete) ||
            (msg.TypeOfEvent == TypeEvent.SettingRename) ||
            (msg.TypeOfEvent == TypeEvent.SwapRankOfTwoSettings) ||
            (msg.TypeOfEvent == TypeEvent.DeleteAllSettingsOfOneFolder)
            )
            SetMarkGridNeedsToBeRefreshed(true);
    }

    private void SetMarkGridNeedsToBeRefreshed(bool NeedToRefresh)
    {
      if (NeedToRefresh)
      {
        if (Form.BxGridRefresh.Image != Form.BxGridRefreshTwo.Image) Form.BxGridRefresh.Image = Form.BxGridRefreshTwo.Image;
      }
      else
      {
        if (Form.BxGridRefresh.Image != Form.BxGridRefreshOne.Image) Form.BxGridRefresh.Image = Form.BxGridRefreshOne.Image;
      }
    }
  }
}


/*


    private void NOT_USED_UpdateSettingValue(Notification msg)
    {
      VxGridSettings.UpdateOneCellValue(CurrentIdFolder, msg.IdSetting, msg.Value);
    }

    private void NOT_USED_UpdateSettingValueUsingTimer(Notification msg)
    {
      Tuple<long, string> t = new Tuple<long, string>(CurrentIdFolder, msg.IdSetting);
      DcSettings[t] = msg.Value;
      if (TmSettings.Enabled == false) TmSettings.Start();
    }

    private void NOT_USED_EventTimerSettingUpdate(object sender, EventArgs e)
    {
      long IdFolder = CurrentIdFolder;
      foreach (var item in DcSettings)
      {
        if (IdFolder != CurrentIdFolder) break;
        if (item.Key.Item1 == CurrentIdFolder)
          VxGridSettings.UpdateOneCellValue(CurrentIdFolder, item.Key.Item2, item.Value);
      }
      DcSettings.Clear();
      TmSettings.Stop();
    }


*/