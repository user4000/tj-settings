﻿using System.Drawing;
using Telerik.WinControls.UI;

namespace TJSettingsFormTreeView
{
  public class TreeviewControlManager
  {
    internal RadTreeView Treeview { get; private set; } = null;

    internal Font FontOfNode { get; private set; } = new Font("Verdana", 9);

    internal ManagerFormTreeView Manager { get; private set; } = null;

    private TreeviewControlManager(ManagerFormTreeView manager)
    {
      Manager = manager;
      FontOfNode = manager.Settings.TreeViewFont;
      Treeview = manager.RfFormTreeView.TvFolders;
      Treeview.ImageList = manager.RfFormTreeView.ImageListFolders;
      SetEvents();
    }

    internal static TreeviewControlManager Create(ManagerFormTreeView manager) => new TreeviewControlManager(manager);

    private void SetEvents()
    {
      Treeview.SelectedNodeChanging += EventSelectedNodeChanging;
    }

    private void EventSelectedNodeChanging(object sender, RadTreeViewCancelEventArgs e)
    {
      e.Cancel = !Manager.TvFormMaintainer.FlagAllowChangeSelectedFolder;
    }

    internal void SetFontAndImageForAllNodes()
    {
      foreach (var item in Treeview.Nodes) SetFontAndImageForOneNode(item);
    }

    private void SetFontAndImageForOneNode(RadTreeNode node)
    {
      node.ImageIndex = node.Nodes.Count == 0 ? 0 : node.Level + 1;
      node.Font = FontOfNode; // node.Text = node.Text + " => " + node.Level;
      if (node.Level < 1) node.Expand();
      foreach (var item in node.Nodes) SetFontAndImageForOneNode(item);
    }

    internal void TryToSelectFolderAfterCreating(RadTreeNode parent, string NameChildFolder)
    {
      RadTreeNode[] nodes = parent.FindNodes(item => item.Name == NameChildFolder);
      if (nodes.Length < 1) return;
      if (Manager.Settings.SelectNewFolderAfterCreating)
        try
        {
          nodes[0].Selected = true; nodes[0].EnsureVisible();
        }
        catch { }
      else
      {
        parent.Expanded = true; parent.Selected = true;
        try
        {
          foreach (RadTreeNode item in parent.Nodes) if (item != nodes[0]) item.Collapse();
          nodes[0].EnsureVisible();
        }
        catch { }
      }
    }
  }
}
