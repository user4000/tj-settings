﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using TJSettings;
using TJStandard;
using static TJFramework.TJFrameworkManager;

namespace TJSettingsFormTreeView
{
  public class GridSettings : GridManager
  {
    internal Timer TmGridSize { get; } = new Timer();

    internal int WidthOfColumnSettingTypeName { get; } = 90;

    internal GridViewTextBoxColumn CnSettingTypeName { get; set; } = null;

    internal ManagerFormTreeView Manager { get; private set; } = null;

    internal string ColumnNameIdSetting { get; } = string.Empty;

    internal string ColumnNameIdFolder { get; } = string.Empty;

    internal string ColumnNameSettingValue { get; } = string.Empty;

    public GridSettings(ManagerFormTreeView manager)
    {
      Manager = manager;
      ColumnNameIdFolder = Standard.GetGridColumnName(nameof(Setting.IdFolder));
      ColumnNameIdSetting = Standard.GetGridColumnName(nameof(Setting.IdSetting));
      ColumnNameSettingValue = Standard.GetGridColumnName(nameof(Setting.SettingValue));
    }

    internal BindingList<Setting> Empty { get; } = new BindingList<Setting>();

    internal BindingList<Setting> ListDataSource { get; set; } = new BindingList<Setting>();

    internal GridViewRowInfo ChangedRow { get; private set; } = null;

    internal string CvAction { get; set; } = string.Empty;

    internal string CvIdFolder { get; set; } = string.Empty;

    internal string CvIdSetting { get; set; } = string.Empty;

    internal bool BxEventCellValueChanged { get; set; } = true;

    internal string[] Password { get; } = { string.Empty, "* * *" };

    public override void SetDataViewControlProperties()
    {
      CreateColumns();
      Grid.DataSource = ListDataSource;
      Grid.ReadOnly = true;
      Grid.AllowEditRow = false;
      Grid.AllowRowResize = false;
      Grid.MultiSelect = false;
      Grid.AllowSearchRow = false;
      Grid.EnableFiltering = false;
      Grid.EnableGrouping = false;
      Grid.EnableSorting = false;
      Grid.HideSelection = true;

      Grid.CurrentColumnChanged += GridCurrentColumnChanged;
      Grid.SelectionMode = GridViewSelectionMode.FullRowSelect;
      Grid.RowFormatting += new RowFormattingEventHandler(EventRowFormatting);
      Grid.CurrentRowChanging += new CurrentRowChangingEventHandler(EventCurrentRowChanging);

      TmGridSize.Interval = 2000;
      TmGridSize.Tick += EventGridSizeTimer;
      TmGridSize.Enabled = false;

      Grid.ClientSizeChanged += EventGridClientSizeChanged;
      Grid.Columns[1].PropertyChanged += EventColumnPropertyChanged;
      Grid.Columns[3].PropertyChanged += EventColumnPropertyChanged;

      //Grid.CurrentCellChanged += GridCurrentCellChanged;
      //Grid.CellFormatting += new CellFormattingEventHandler(EventCellFormatting);
      //Grid.CellValueChanged += EventCellValueChanged;
      //SetThemeForGrid();
    }

    internal void CreateColumns()
    {
      //----------------------------------------------------------------------------------------------------------------------------------------
      AddColumn<GridViewTextBoxColumn>(nameof(Setting.IdFolder), "IdFolder hidden", true, typeof(int));
      //----------------------------------------------------------------------------------------------------------------------------------------
      AddColumn<GridViewTextBoxColumn>(nameof(Setting.IdSetting), "setting", true, typeof(string), 250);
      //----------------------------------------------------------------------------------------------------------------------------------------    
      var CnSettingIdType = AddColumn<GridViewTextBoxColumn>(nameof(Setting.IdType), "IdType hidden", true, typeof(int));
      //----------------------------------------------------------------------------------------------------------------------------------------    
      CnSettingTypeName = AddColumn<GridViewTextBoxColumn>(nameof(Setting.NameType), "type", true, typeof(string), WidthOfColumnSettingTypeName);
      //----------------------------------------------------------------------------------------------------------------------------------------
      var CnSettingValue = AddColumn<GridViewTextBoxColumn>(nameof(Setting.SettingValue), "value", true, typeof(string), 400);
      //----------------------------------------------------------------------------------------------------------------------------------------
      AddColumn<GridViewTextBoxColumn>(nameof(Setting.Rank), "rank hidden", true, typeof(int));
      //----------------------------------------------------------------------------------------------------------------------------------------
      var CnBooleanValue = AddColumn<GridViewTextBoxColumn>(nameof(Setting.BooleanValue), "boolean value hidden", true, typeof(string));
      //----------------------------------------------------------------------------------------------------------------------------------------

      ExpressionFormattingObject obj = new ExpressionFormattingObject("Boolean_Value_False", $"{CnBooleanValue.Name} = '0'", false);
      obj.CellBackColor = Color.FromArgb(255, 229, 229);
      CnSettingValue.ConditionalFormattingObjectList.Add(obj);

      obj = new ExpressionFormattingObject("Boolean_Value_True", $"{CnBooleanValue.Name} = '1'", false);
      obj.CellBackColor = Color.FromArgb(209, 255, 209); ;
      CnSettingValue.ConditionalFormattingObjectList.Add(obj);

      obj = new ExpressionFormattingObject("Integer", $"{CnSettingIdType.Name} = {(int)(TypeSetting.Integer64)}", false);
      obj.CellForeColor = Color.Blue;
      CnSettingValue.ConditionalFormattingObjectList.Add(obj);

      obj = new ExpressionFormattingObject("TypeName", "0 = 0", false);
      obj.CellForeColor = Color.DarkGray;
      CnSettingTypeName.ConditionalFormattingObjectList.Add(obj);

      //Grid.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
      //AddSorting(nameof(Setting.Rank));
    }

    private void AdjustColumnWidth()
    {
      int width = -20 + Grid.ClientSize.Width - Grid.Columns[1].Width - Grid.Columns[3].Width;
      if (Grid.Columns[4].Width != width) Grid.Columns[4].Width = width;
    }

    private void EventGridSizeTimer(object sender, EventArgs e)
    {
      AdjustColumnWidth(); TmGridSize.Stop();
    }

    private void EventColumnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "Width") EventGridClientSizeChanged(sender, e);
    }

    private void EventGridClientSizeChanged(object sender, EventArgs e)
    {
      if (TmGridSize.Enabled == false) TmGridSize.Start();
    }

    private void EventCurrentRowChanging(object sender, CurrentRowChangingEventArgs e)
    {
      e.Cancel = !Manager.TvFormMaintainer.FlagAllowChangeSelectedFolder;
    }

    private void GridCurrentColumnChanged(object sender, CurrentColumnChangedEventArgs e)
    {
      try { if (e.NewColumn.Index > 1) Grid.CurrentColumn = Grid.Columns[1]; } catch { }
    }

    private void EventCellValueChanged(object sender, GridViewCellEventArgs e)
    {
      if (BxEventCellValueChanged == false) return;
      ChangedRow = Grid.Rows[e.RowIndex];
      CvAction = ChangedRow.Cells[e.ColumnIndex].Value.ToString();
      CvIdFolder = ChangedRow.Cells[Standard.GetGridColumnName(nameof(Setting.IdFolder))].Value.ToString();
      CvIdSetting = ChangedRow.Cells[Standard.GetGridColumnName(nameof(Setting.IdSetting))].Value.ToString();
    }

    internal void ResetView()
    {
      RefreshGrid(Empty);
    }

    private void RefreshGrid()
    {
      Grid.DataSource = ListDataSource;

      if (Grid.Rows.Count > 0)
      {
        Grid.GridNavigator.ClearSelection(); // Clear selection //
      }

      //if (TmGridSize.Enabled==false) AdjustColumnWidth();
    }

    internal void HidePasswordValues()
    {
      for (int i = 0; i < ListDataSource.Count; i++)
        if (ListDataSource[i].IdType == (int)TypeSetting.Password)
          ListDataSource[i].SettingValue = Password[Math.Sign(ListDataSource[i].SettingValue.Length)];
    }

    internal void RefreshGrid(BindingList<Setting> list)
    {
      Grid.DataSource = null;
      if ((list != null) && (list.Count > 0))
      {
        ListDataSource = list;
        HidePasswordValues();
      }
      else
      {
        ListDataSource = Empty;
      }
      RefreshGrid();
    }

    internal string GetIdSetting() => this.GetStringValue(nameof(Setting.IdSetting));

    internal Setting GetSetting(string IdSetting)
    {
      if (ListDataSource != null)
        foreach (var item in ListDataSource)
          if (item.IdSetting == IdSetting) return item;
      return null;
    }

    internal int GetRank(Setting setting) => setting == null ? -1 : setting.Rank;

    internal Setting UpperSibling(Setting setting)
    {
      Setting found = null;
      foreach (var item in ListDataSource)
        if (item.Rank < setting.Rank)
          if (item.Rank > GetRank(found))
            found = item;
      return found;
    }

    internal Setting LowerSibling(Setting setting)
    {
      Setting found = null;
      foreach (var item in ListDataSource)
        if (item.Rank > setting.Rank)
          if (item.Rank < (GetRank(found) < 0 ? int.MaxValue : GetRank(found)))
            found = item;
      return found;
    }

    internal void SwapRank(Setting SelectedSetting, Setting Sibling)
    {
      if ((SelectedSetting == null) || (Sibling == null)) return;

    }

    internal void SelectRow(string IdSetting)
    {
      for (int i = 0; i < Grid.Rows.Count; i++)
      {
        if (Grid.Rows[i].Cells[ColumnNameIdSetting].Value.ToString() == IdSetting)
          if (!Grid.Rows[i].IsSelected)
          {
            Grid.Rows[i].IsSelected = true;
            Grid.Rows[i].IsCurrent = true;
            break;
          }
      }
    }

    internal void UpdateOneCellValue(long IdFolder, string IdSetting, string Value)
    {
      for (int i = 0; i < Grid.Rows.Count; i++)
      {
        if (Grid.Rows[i].Cells[ColumnNameIdFolder].Value.ToString() != IdFolder.ToString()) break;
        if (Grid.Rows[i].Cells[ColumnNameIdSetting].Value.ToString() == IdSetting)       
        {
          Grid.Rows[i].Cells[ColumnNameSettingValue].Value = Value;
          Ms.Message($"{IdSetting} --- {Value}", $"{Grid.Rows[i].Cells[ColumnNameSettingValue].Value}").NoAlert().Debug();
          break;
        }
      }
    }
  }
}

