﻿using System;

namespace TJStandard
{
  [Serializable]
  public class ReturnCode
  {
    public int Number { get; set; } = -1; // Numeric part of the ReturnCode. If Number==0 then SUCCESS else FAILURE //

    public long IdObject { get; set; } = -1; // Contains Identifier of an object, affected by some action //

    public string Message { get; set; } = string.Empty; 

    public string Note { get; set; } = string.Empty;

    public ReturnCode() { /* CONSTRUCTOR */ }

    /* CONSTRUCTOR */
    public ReturnCode(int numericValue, long idObject, string stringValue, string stringNote)
    {
      Number = numericValue; IdObject = idObject; Message = stringValue; Note = stringNote;
    }

    public bool Success { get => Number == 0; }

    public bool Error { get => Number != 0; }
  }
}

